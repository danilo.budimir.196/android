package com.budimir.danilo.gradjevinskafirma.Notifications;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.android.volley.toolbox.NetworkImageView;
import com.budimir.danilo.gradjevinskafirma.Activitys.ProfileActivity;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;

import java.util.Random;
import java.util.concurrent.ExecutionException;



public class NewRecuiermentNotification {


    @RequiresApi(api = Build.VERSION_CODES.O)
    private static String createNotificationChannel(String chanelId, String channelName, Context context) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationChannel chanel = new NotificationChannel(chanelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        chanel.setLightColor(Color.BLUE);
        chanel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        chanel.enableVibration(false);
        chanel.setVibrationPattern(new long[]{1000,500,1000});
        chanel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        chanel.setSound(alarmSound,  new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setLegacyStreamType(AudioManager.STREAM_NOTIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT).build());

        chanel.enableLights(true);
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(chanel);


        return chanelId;


    }



    public static void createNotification(String chanelId, String channelName, Context context, RequiermentListItem item)
    {

        FutureTarget<Bitmap> futureimg = Glide.with(context).asBitmap().load(ConectionInfo.getImagePath()+item.getUserImage()).submit();
        Bitmap img = null;
        try {
             img = futureimg.get();
            // Log.d("SLIKA","uspela");
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String chaneelId;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            chaneelId = createNotificationChannel(chanelId,channelName,context);
        }else {

            chaneelId = "";
        }


        Intent resultIntent= new Intent(context,ProfileActivity.class);
        resultIntent.putExtra("RequiermentFragment","REQUIERMENT");
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

        //img = BitmapFactory.decodeResource(context.getResources(),R.drawable.avatar_img);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,chaneelId);
         builder.setOngoing(false).
                setContentText(item.getRequiermentTitele()).setContentTitle(item.getRequierUsername() + " "+item.getRequierSurname()).
                setSmallIcon(R.drawable.newrequierment_small).
                setCategory(Notification.CATEGORY_SERVICE).
                setLargeIcon(img).
                setVisibility(Notification.VISIBILITY_PUBLIC).setAutoCancel(true);
                builder.setSound(alarmSound);
                builder.setVibrate(new long[] {0,1000,1000,1000,1000});
                builder.setOnlyAlertOnce(true);
                builder.setAutoCancel(true);
                 builder.setContentIntent(resultPendingIntent);

                Notification notification = builder.build();
                notification.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS;


        manager.notify(item.getRequiermentId(),notification);
    }

}