package com.budimir.danilo.gradjevinskafirma.Interfaces;

import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;

import java.util.List;

public interface LoadRequiermentsCallBack {

    void getRequiermentsCallBack(List<RequiermentListItem> requierments);
}
