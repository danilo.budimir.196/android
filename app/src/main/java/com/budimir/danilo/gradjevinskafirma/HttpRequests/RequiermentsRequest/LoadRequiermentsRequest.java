package com.budimir.danilo.gradjevinskafirma.HttpRequests.RequiermentsRequest;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.Fragments.Requierments;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Interfaces.LoadRequiermentsCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class LoadRequiermentsRequest {
    private final String GET_REQUIERMENTS_TAG = "GET_REQUIERMENTS";
    private final String GET_REQUIERMENTS_URL = ConectionInfo.getConnAddres()+"/getRequierments";
    private static LoadRequiermentsRequest instance =null;

    private LoadRequiermentsRequest(){}

    public static synchronized LoadRequiermentsRequest getInstance()
    {
        if (instance ==null) instance = new LoadRequiermentsRequest();
        return instance;
    }


    public void pullRequierments(final Context context, final LoadRequiermentsCallBack callBack){

        JsonObjectRequest getRequiermentsRequeest = new JsonObjectRequest(Request.Method.GET, GET_REQUIERMENTS_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("is_success")) {
                        final List<RequiermentListItem> responseList =jsonArrayToArrayList(response.getJSONArray("requierments"));
                        callBack.getRequiermentsCallBack(responseList);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            public Map getHeaders() throws AuthFailureError {
                HashMap heders = new HashMap();
                Token token = new Token();
                token.id = 1;
                token.funcCaller = TokenCrudCaled.getToken;
                token.token_value = null;
                try {
                    token = (Token) new TokenAsyncTask(context).execute(new Token[]{token}).get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                heders.put("Content-Type", "application/json");
                heders.put("Authorization", token.token_value);
                return heders;
            }



        };

        getRequiermentsRequeest.setTag(GET_REQUIERMENTS_TAG);
        HttpSinglton.getInstance(context).addToRequestQueue(getRequiermentsRequeest);

    }


    private List<RequiermentListItem> jsonArrayToArrayList(JSONArray requierments) {
        List<RequiermentListItem> requiermentList = new ArrayList<>();

        for (int i = 0;i<requierments.length(); i++){
            try {
                int requiermentID = ((JSONObject) requierments.get(i)).getInt("requierment_id");
                String apartmentName = ((JSONObject) requierments.get(i)).getString("apartment_name");
                String requiermentTitle = ((JSONObject) requierments.get(i)).getString("title");
                String user_name = ((JSONObject) requierments.get(i)).optString("user_name");
                String user_surname = ((JSONObject) requierments.get(i)).getString("user_surname");
                JSONObject image = ((JSONObject) requierments.get(i)).getJSONObject("user_image");
                String strImage = "";
                if (image.getBoolean("Valid")) {
                    strImage = image.getString("String");
                }
                requiermentList.add(new RequiermentListItem(requiermentID, requiermentTitle, apartmentName, "", strImage, user_name, user_surname));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return requiermentList;
    }

}
