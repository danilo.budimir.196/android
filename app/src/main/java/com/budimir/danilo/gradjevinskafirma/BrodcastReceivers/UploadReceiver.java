package com.budimir.danilo.gradjevinskafirma.BrodcastReceivers;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.budimir.danilo.gradjevinskafirma.Adapters.RequiermentsAdapter;
import com.budimir.danilo.gradjevinskafirma.Fragments.Requierments;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.RequiermentsViewModel;

public class UploadReceiver extends BroadcastReceiver {


    public static final String BROADCAST_ACTION = "UPLOAD_RECEIVER";
    private RequiermentsViewModel model;


    public UploadReceiver(RequiermentsViewModel model){
        this.model = model;

    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle fromService = intent.getExtras();
       if (fromService.getBoolean("is_success"))
       {
           RequiermentListItem item = (RequiermentListItem) fromService.getSerializable("item");
           if(item!=null)model.addRequierment(item);
           Toast.makeText(context,"Images Uploaded",Toast.LENGTH_SHORT).show();

       }else {
           Toast.makeText(context,"Images Upload faild",Toast.LENGTH_SHORT).show();
       }

    }


}
