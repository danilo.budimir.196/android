package com.budimir.danilo.gradjevinskafirma.Interfaces;

import com.budimir.danilo.gradjevinskafirma.Models.ChooseApartmentItem;

import java.util.List;

public interface LoadApartmentsListCallBack {

    void getApartmentsListCallBack(List<ChooseApartmentItem> item);
}
