package com.budimir.danilo.gradjevinskafirma.Fragments;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.budimir.danilo.gradjevinskafirma.Activitys.ProfileActivity;
import com.budimir.danilo.gradjevinskafirma.Adapters.RequiermentsAdapter;
import com.budimir.danilo.gradjevinskafirma.BrodcastReceivers.UploadReceiver;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.RequiermentsViewModel;
import com.budimir.danilo.gradjevinskafirma.R;
import java.util.ArrayList;
import java.util.Objects;

public class Requierments extends Fragment {

    private RequiermentsViewModel requiermentsViewModel;
    private ListView requiermentsView;
    private IntentFilter uploadFilter;
    private UploadReceiver uploadReceiver;
    private static RequiermentsAdapter adapter;




    public Requierments() {

    }





    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requiermentsViewModel = ViewModelProviders.of(getActivity()).get(RequiermentsViewModel.class);
        uploadFilter = new IntentFilter(UploadReceiver.BROADCAST_ACTION);
        uploadReceiver = new UploadReceiver(requiermentsViewModel);
        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(uploadReceiver,uploadFilter);

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_requierments, container, false);
        requiermentsView = (ListView) rootView.findViewById(R.id.requiermentsList);
        requiermentsViewModel.getRequierments().observe(this, new Observer<ArrayList<RequiermentListItem>>() {
            @Override
            public void onChanged(@Nullable final ArrayList<RequiermentListItem> requiermentListItems) {

                Objects.requireNonNull(Requierments.this.getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new RequiermentsAdapter(rootView.getContext(), 0, requiermentListItems);
                       requiermentsView.setAdapter(adapter);
                        requiermentsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                ((ProfileActivity) Objects.requireNonNull(getActivity())).showSinglerequiermentFragemnt(requiermentListItems.get(position).getRequiermentId());
                                Log.d("ID_U_R","id ="+requiermentListItems.get(position).getRequiermentId());


                            }
                        });


                    }
                });
            }
        });






        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    public static void refresh()
    {
        adapter.notifyDataSetChanged();
    }









    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(uploadReceiver);
    }


}
