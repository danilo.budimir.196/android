package com.budimir.danilo.gradjevinskafirma.Services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.budimir.danilo.gradjevinskafirma.Activitys.UploadAactivity;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.BrodcastReceivers.UploadReceiver;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.Interfaces.IuploadAPI;
import com.budimir.danilo.gradjevinskafirma.Interfaces.UploadCallBacks;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.budimir.danilo.gradjevinskafirma.Utils.ProgressRequestBody;
import com.budimir.danilo.gradjevinskafirma.Utils.RetrofitClient;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.budimir.danilo.gradjevinskafirma.Notifications.UploadNotification.CHANNEL_ID;

public class UploadImageService extends IntentService implements UploadCallBacks {

    private int numofImages;
    private final String UPLOAD_IMAGE_URL = ConectionInfo.getRetrofitBaseUrl();
    IuploadAPI iuploadAPI;
    private int uploadedImages = 0;

    private IuploadAPI getIuploadAPI() {
        return (IuploadAPI) RetrofitClient.getClient(this.UPLOAD_IMAGE_URL).create(IuploadAPI.class);
    }


    public UploadImageService() {
        super("UPLOAD_SERVICE");
        setIntentRedelivery(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        String chaneelId;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            chaneelId = createNotificationChannel("IMAGES_UPLOAD","Images Uploading");
        }else {

            chaneelId = "";
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,chaneelId);
        Notification notification = builder.setOngoing(true).setContentText("Uploading Images").setSmallIcon(R.drawable.ic_launcher_background).setCategory(Notification.CATEGORY_SERVICE).build();
        startForeground(1,notification);

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        this.iuploadAPI = getIuploadAPI();
        ArrayList<String> imagePaths = intent.getExtras().getStringArrayList("imagesForUpload");
        RequiermentListItem item = (RequiermentListItem)intent.getExtras().getSerializable("item");
        this.numofImages = imagePaths.size();
        int imagesUploaded =0;
        Iterator it = imagePaths.iterator();

        while (it.hasNext())
        {
            String path = (String) it.next();
            uploadImage(path);
            imagesUploaded++;
        }

        if (numofImages==imagesUploaded) startBrodacast(true,item);
        else startBrodacast(false,null);



    }



    private void uploadImage(String imagePath) {
        File file = new File(imagePath);
        final MultipartBody.Part body = MultipartBody.Part.createFormData("img", file.getName(), new ProgressRequestBody(file, this));
        Token token = new Token();
        token.id = 1;
        token.funcCaller = TokenCrudCaled.getToken;
        Token info = null;
        try {
            info = (Token) new TokenAsyncTask(this).execute(new Token[]{token}).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        final Token finalInfo = info;

            this.iuploadAPI.uploadFile(body, finalInfo.token_value).enqueue(new Callback<String>() {
                    public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                    }

                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onProgresUpdate(int i) {

    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    private String createNotificationChannel(String chanelId, String channelName)
    {
        NotificationChannel chanel = new NotificationChannel(chanelId,channelName,NotificationManager.IMPORTANCE_NONE);
        chanel.setLightColor(Color.BLUE);
        chanel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(chanel);

        return chanelId;

    }

    private void startBrodacast(boolean success, RequiermentListItem item)
    {
        Intent broadcastIntnen = new Intent(UploadReceiver.BROADCAST_ACTION);
        broadcastIntnen.putExtra("is_success", success);
        broadcastIntnen.putExtra("item",item);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntnen);

    }
}
