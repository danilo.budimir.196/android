package com.budimir.danilo.gradjevinskafirma.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;

import com.budimir.danilo.gradjevinskafirma.DatabaseConf.AppDatabase;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.UserDetails;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;

public class UserDetailsAsyncTask extends AsyncTask<UserDetails,Void,UserDetails> {

    private Context context;
    private UserDetails userDetails = new UserDetails();

    public UserDetailsAsyncTask(Context context)
    {
        this.context = context;

    }


    @Override
    public UserDetails doInBackground(UserDetails... userDetails) {
        if(userDetails.length>1)
        {
            UserDetails details = this.userDetails;
            details.executeStatus = false;
            return details;
        }

        switch (userDetails[0].funcCaller)
        {
            case insertUser:
                AppDatabase.getAppDatabase(this.context).userDetailsDao().insert(userDetails[0]);
                AppDatabase.destroyInstance();
                this.userDetails.executeStatus = true;
                break;
            case deleteUser:
                AppDatabase.getAppDatabase(this.context).userDetailsDao().delete(userDetails[0]);
                AppDatabase.destroyInstance();
                this.userDetails.executeStatus = true;
                break;
            case getUser:
                this.userDetails = AppDatabase.getAppDatabase(this.context).userDetailsDao().getUserDetails(userDetails[0].id);
                AppDatabase.destroyInstance();
                this.userDetails.executeStatus = true;
                break;
            case updateUser:
                AppDatabase.getAppDatabase(this.context).userDetailsDao().update(userDetails[0]);
                AppDatabase.destroyInstance();
                this.userDetails.executeStatus = true;
                break;

        }

        return this.userDetails;
    }
}
