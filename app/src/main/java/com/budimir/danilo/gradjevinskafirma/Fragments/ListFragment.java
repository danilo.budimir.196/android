package com.budimir.danilo.gradjevinskafirma.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.Adapters.ApartmentAdapter;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.ApartmentsRequests.LoadApartmentsRequests;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Models.ApartmentListItem;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.ApartmentsViewModel;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListFragment extends Fragment {

    private ApartmentsViewModel apartments_model;

    public ListFragment() {

    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_list, container, true);
        apartments_model = ViewModelProviders.of(getActivity()).get(ApartmentsViewModel.class);
        apartments_model.getApartments().observe( this, new Observer<ArrayList<ApartmentListItem>>() {
            @Override
            public void onChanged(@Nullable final ArrayList<ApartmentListItem> item) {

                ListFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ListView) rootView.findViewById(R.id.apartmentList)).setAdapter(new ApartmentAdapter(ListFragment.this.getActivity(),0,item));
                    }
                });

            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void onDetach() {
        LoadApartmentsRequests.getInstance().rewoveFromQue(ListFragment.this.getActivity());
        super.onDetach();
    }
}
