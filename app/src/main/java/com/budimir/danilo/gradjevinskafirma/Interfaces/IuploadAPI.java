package com.budimir.danilo.gradjevinskafirma.Interfaces;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface IuploadAPI {
    @POST("/uploadImages")
    @Multipart
    Call<String> uploadFile(@Part MultipartBody.Part part, @Header("Authorization") String str);
}
