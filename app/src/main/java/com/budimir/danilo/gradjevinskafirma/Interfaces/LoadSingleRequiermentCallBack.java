package com.budimir.danilo.gradjevinskafirma.Interfaces;

import com.budimir.danilo.gradjevinskafirma.Fragments.SingleRequierment;
import com.budimir.danilo.gradjevinskafirma.Models.SingleRequiermentItem;

public interface LoadSingleRequiermentCallBack {

    void getSingleRequiermentCallBack(SingleRequiermentItem requierment);
}
