package com.budimir.danilo.gradjevinskafirma.Activitys;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenu;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.budimir.danilo.gradjevinskafirma.BrodcastReceivers.NewRequiermentBroadcastReceiver;
import com.budimir.danilo.gradjevinskafirma.BrodcastReceivers.UploadReceiver;
import com.budimir.danilo.gradjevinskafirma.Fragments.AddReqiermentFragment;
import com.budimir.danilo.gradjevinskafirma.Fragments.ImagePreviewFragment;
import com.budimir.danilo.gradjevinskafirma.Fragments.ProfileFragment;
import com.budimir.danilo.gradjevinskafirma.Fragments.Requierments;
import com.budimir.danilo.gradjevinskafirma.Fragments.SingleRequierment;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.RequiermentsViewModel;
import com.budimir.danilo.gradjevinskafirma.R;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.ArrayList;


public class ProfileActivity extends AppCompatActivity {
    private static Fragment displayedFragment = new ProfileFragment();
    private final  String PROFILEFRAGMENT_TAG= "PROFILE";
    private final  String REQUIERMENTS_TAG = "REQUIERMENTS";
    private final  String ADD_REQUIERMENTS_TAG = "ADD";
    public BottomNavigationView navView;
    private boolean ifHidden = false;
    private RequiermentsViewModel requiermentsViewModel;
    private IntentFilter uploadFilter;
    private IntentFilter newRequiermentFilter;
    private NewRequiermentBroadcastReceiver newRequiermentBroadcastReceiver;
    private int notificationCounts = 0;
    private boolean badgevisible = false;
    private View notificationBadge;
    NotificationBadge badge;


    private OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new OnNavigationItemSelectedListener() {
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
             FragmentTransaction transaction = ProfileActivity.this.getSupportFragmentManager().beginTransaction();
            int itemId = item.getItemId();
            if (itemId == R.id.addRequierments) {
                setAddRequiermentFragment(transaction);
                return true;
            } else if (itemId == R.id.profile) {
                setProfileFragment(transaction);
                return true;
            } else if (itemId == R.id.requierments) {
                setRequiermentsFragment(transaction);
                requiermentsViewModel.setRecuiermentCount(0);
                return true;
            } else {

                return true;
            }
        }
    };

    /* Access modifiers changed, original: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_profile);
        navView = (BottomNavigationView) findViewById(R.id.navigation);
        navView.setOnNavigationItemSelectedListener(this.mOnNavigationItemSelectedListener);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.output, displayedFragment,PROFILEFRAGMENT_TAG).addToBackStack("profile_back_stack");
        transaction.commit();

        requiermentsViewModel = ViewModelProviders.of(this).get(RequiermentsViewModel.class);
        uploadFilter = new IntentFilter(UploadReceiver.BROADCAST_ACTION);
        newRequiermentFilter = new IntentFilter(NewRequiermentBroadcastReceiver.BROADCAST_ACTION);

        newRequiermentBroadcastReceiver = new NewRequiermentBroadcastReceiver(requiermentsViewModel);

        LocalBroadcastManager.getInstance(this).registerReceiver(newRequiermentBroadcastReceiver,uploadFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(newRequiermentBroadcastReceiver,newRequiermentFilter);
        addBadgeToRequierments();

        requiermentsViewModel.getRecuiermentCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                Log.d("NUM OF NOT","NULL je");
                if (integer!=null)badge.setNumber(integer,true);
                else {
                    Log.d("NUM OF NOT","NULL je");
                }
            }
        });




    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("DBG","ONNEWINTENT");
        String requiermentFragment = intent.getStringExtra("RequiermentFragment");

        if(requiermentFragment != null) {
            if (requiermentFragment.equals("REQUIERMENT")) {
                setContentView(R.layout.fragment_requierments);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                setRequiermentsFragment(transaction);
            }
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }


    public void onDestroy() {
        super.onDestroy();
    }


    private void setAddRequiermentFragment(FragmentTransaction transaction)
    {
        transaction.detach(displayedFragment);
        Fragment add_reqierment_fragment  = getSupportFragmentManager().findFragmentByTag(ADD_REQUIERMENTS_TAG);

        if (add_reqierment_fragment != null)
        {
            displayedFragment = add_reqierment_fragment;
            transaction.attach(add_reqierment_fragment);

        }else {
            displayedFragment = new AddReqiermentFragment();
            transaction.add(R.id.output,displayedFragment,ADD_REQUIERMENTS_TAG).addToBackStack("add_requierment_back_stack");


        }

        transaction.commit();

    }

    private void setProfileFragment(FragmentTransaction transaction){
        transaction.detach(displayedFragment);

        Fragment profile_fragment = getSupportFragmentManager().findFragmentByTag(PROFILEFRAGMENT_TAG);

        if (profile_fragment != null)
        {
            displayedFragment = profile_fragment;
            transaction.attach(profile_fragment);

        }else {

            displayedFragment = new ProfileFragment();
            transaction.add(R.id.output,displayedFragment,PROFILEFRAGMENT_TAG).addToBackStack("profile_back_stack");

        }

        transaction.commit();

    }


    private void setRequiermentsFragment(FragmentTransaction transaction)
    {
        transaction.detach(displayedFragment);
        Fragment requierments_fragment = getSupportFragmentManager().findFragmentByTag(REQUIERMENTS_TAG);

        if (requierments_fragment != null)
        {
        displayedFragment = requierments_fragment;
        transaction.attach(requierments_fragment);
        Log.d("SET_REQUIERMENT_if","requierment_if");

        }else {
        displayedFragment = new Requierments();
        transaction.add(R.id.output,displayedFragment,REQUIERMENTS_TAG).addToBackStack("requierment_backstack");
        Log.d("SET_REQUIERMENT_else","requierment_else");

        }
        transaction.commit();
    }


    public void setRequiermentFragmentAfterUpload()
    {
        FragmentTransaction transaction = ProfileActivity.this.getSupportFragmentManager().beginTransaction();
        transaction.detach(displayedFragment);
        Fragment requierments_fragment = getSupportFragmentManager().findFragmentByTag(REQUIERMENTS_TAG);

        if (requierments_fragment != null)
        {
            displayedFragment = requierments_fragment;
            transaction.attach(requierments_fragment);
            Log.d("UPLOAD_IF","requierment_if");

        }else {
            displayedFragment = new Requierments();
            transaction.add(R.id.output,displayedFragment,REQUIERMENTS_TAG).addToBackStack("requierment_backstack");
            Log.d("UPLOAD_else","requierment_else");

        }
        transaction.commit();

    }



    public void showSinglerequiermentFragemnt(int id)
    {
        Log.d("ID_U_PA","id ="+id);
        FragmentTransaction transaction = ProfileActivity.this.getSupportFragmentManager().beginTransaction();
        Fragment single_requierment_fragment = getSupportFragmentManager().findFragmentByTag("SINGLE_REQUIERMENT_FRAGMENT");
        transaction.detach(displayedFragment);

        if(single_requierment_fragment != null)
        {
            displayedFragment =single_requierment_fragment;
            Bundle bundle = new Bundle();
            bundle.putInt("id",id);
            displayedFragment.setArguments(bundle);
            transaction.attach(displayedFragment);
        }else
        {
            displayedFragment = new SingleRequierment();
            Bundle bundle = new Bundle();
            bundle.putInt("id",id);
            displayedFragment.setArguments(bundle);
            transaction.add(R.id.output, displayedFragment,"SINGLE_REQUIERMENT_FRAGMENT").addToBackStack("SIngleItemRequierment");

        }

        transaction.commit();

    }


    public void showImagePrview(int id, ArrayList<String> imagesUrls)
    {
        FragmentTransaction transaction = ProfileActivity.this.getSupportFragmentManager().beginTransaction();
        transaction.detach(displayedFragment);
        Fragment image_preview = getSupportFragmentManager().findFragmentByTag("IMAGE_PREVIEW");
        navView.clearAnimation();
        navView.animate().translationY(navView.getHeight()).setDuration(300);
        ifHidden = true;
        if(image_preview!=null)
        {
            displayedFragment = image_preview;
            transaction.attach(displayedFragment);


        }else {
            displayedFragment = new ImagePreviewFragment();

            transaction.add(R.id.output,displayedFragment,"IMAGE_PREVIEW").addToBackStack("ImagePreview");

        }

        transaction.commit();
    }

    public void addBadgeToRequierments(){

        BottomNavigationMenuView menu = (BottomNavigationMenuView) navView.getChildAt(0);
        View requiermentView = menu.getChildAt(1);
        BottomNavigationItemView itemView = (BottomNavigationItemView) requiermentView;
        notificationBadge = LayoutInflater.from(this).inflate(R.layout.badge,itemView,false);
        badge = notificationBadge.findViewById(R.id.notifications_badge);
        itemView.addView(notificationBadge);

    }

    public void addNotificationCounts()
    {
        notificationCounts++;
        badge.setNumber(notificationCounts,true);
    }


    public void setNotificotionCountToZerro()
    {
        notificationCounts = 0;
        badge.setNumber(0);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (ifHidden)
        {
            navView.clearAnimation();
            navView.animate().translationY(0).setDuration(300);
            ifHidden =true;
        }
        FragmentTransaction transaction = ProfileActivity.this.getSupportFragmentManager().beginTransaction();
        transaction.detach(displayedFragment);
        transaction.remove(displayedFragment);
        transaction.commit();
        FragmentManager manager = getSupportFragmentManager();
        for (Fragment f : manager.getFragments())
        {
            if(f!=null && f.isVisible()){
                displayedFragment=f;
                break;
            }
        }

        if (manager.getBackStackEntryCount() == 1) {
            this.finish();
            return;
        }

   if (displayedFragment.getTag()!= null) {
       switch (displayedFragment.getTag()) {
           case PROFILEFRAGMENT_TAG:
               navView.setSelectedItemId(R.id.profile);
               break;
           case REQUIERMENTS_TAG:
               requiermentsViewModel.setRecuiermentCount(0);
               navView.setSelectedItemId(R.id.requierments);
               break;
           case ADD_REQUIERMENTS_TAG:
               navView.setSelectedItemId(R.id.addRequierments);
               break;


       }
   }

    }







}
