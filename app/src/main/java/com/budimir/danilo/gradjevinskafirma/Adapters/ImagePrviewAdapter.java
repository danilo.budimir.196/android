package com.budimir.danilo.gradjevinskafirma.Adapters;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.NetworkImageView;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;


import java.util.ArrayList;

import okhttp3.ConnectionPool;

public class ImagePrviewAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> imageUrls;
    private int itemToshow;


    public ImagePrviewAdapter(@NonNull Context context,@NonNull ArrayList<String> imageUrls){
        this.context=context;
        this.imageUrls = imageUrls;
    }




    @Override
    public int getCount() {
        return imageUrls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        NetworkImageView imageView = new NetworkImageView(context);

        imageView.setImageUrl(ConectionInfo.getImagePath()+imageUrls.get(position), HttpSinglton.getInstance(context).getImageLoader());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        container.addView(imageView);
        return imageView;

    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View) object);
    }
}
