package com.budimir.danilo.gradjevinskafirma.Models.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.NonNull;

import com.budimir.danilo.gradjevinskafirma.HttpRequests.ApartmentsRequests.LoadApartmentsRequests;
import com.budimir.danilo.gradjevinskafirma.Interfaces.LoadApartmentsCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.ApartmentListItem;
import com.budimir.danilo.gradjevinskafirma.Models.ChooseApartmentItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ApartmentsViewModel extends AndroidViewModel {

    private ArrayList<ApartmentListItem> aparttmentList = new ArrayList<>();
    private ArrayList<ChooseApartmentItem> chooseApartmentItems = new ArrayList<>();
    private MutableLiveData<ArrayList<ApartmentListItem>> apartmentsListMutable;
    private MutableLiveData<ArrayList<ChooseApartmentItem>> chooseApartmentsMutable;

    public ApartmentsViewModel(@NonNull Application application) {
        super(application);

    }

    public LiveData<ArrayList<ApartmentListItem>> getApartments() {

        if (apartmentsListMutable ==null)
        {

            apartmentsListMutable = new MutableLiveData<ArrayList<ApartmentListItem>>();
            loadAppartments();
        }

        return apartmentsListMutable;
    }


    public LiveData<ArrayList<ChooseApartmentItem>> getChooseApartmentItem()
    {
        if (chooseApartmentsMutable == null) loadChooseAppartments();
        return chooseApartmentsMutable;
    }

    private void loadAppartments(){

        LoadApartmentsRequests.getInstance().pullApartments(super.getApplication().getApplicationContext(), new LoadApartmentsCallBack() {
            @Override
            public void getApartmentsCallBack(List<ApartmentListItem> apartments) {

                aparttmentList.addAll(apartments);
                apartmentsListMutable.setValue(aparttmentList);
            }
        });

    }


    private void loadChooseAppartments()
    {
        if (apartmentsListMutable != null)
        {
            populateChooseApartments();
            chooseApartmentsMutable = new MutableLiveData<>();
            chooseApartmentsMutable.setValue(chooseApartmentItems);
        }else
        {
            chooseApartmentsMutable = new MutableLiveData<>();
            LoadApartmentsRequests.getInstance().pullApartments(super.getApplication().getApplicationContext(), new LoadApartmentsCallBack() {
                @Override
                public void getApartmentsCallBack(List<ApartmentListItem> apartments) {

                    aparttmentList.addAll(apartments);
                    populateChooseApartments();
                    chooseApartmentsMutable.setValue(chooseApartmentItems);
                    apartmentsListMutable.setValue(aparttmentList);
                }
            });

        }
    }


    private void populateChooseApartments()
    {
       for (int i = 0; i<aparttmentList.size();i++)
       {
           chooseApartmentItems.add(new ChooseApartmentItem(aparttmentList.get(i).getApartment_id(),aparttmentList.get(i).getApartment_name()));
       }
    }
















}