package com.budimir.danilo.gradjevinskafirma.Fragments;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.PathInterpolatorCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.Activitys.ProfileActivity;
import com.budimir.danilo.gradjevinskafirma.Activitys.UploadAactivity;
import com.budimir.danilo.gradjevinskafirma.Adapters.ChooseApartmentSpinnerAdapter;
import com.budimir.danilo.gradjevinskafirma.Adapters.ChooseRequiermentTypeAdapter;
import com.budimir.danilo.gradjevinskafirma.Adapters.SliderAdapter;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.AddRequiermentsRequests.SendRequiermentRequest;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Interfaces.IuploadAPI;
import com.budimir.danilo.gradjevinskafirma.Interfaces.SendRequiermentCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.ChooseApartmentItem;
import com.budimir.danilo.gradjevinskafirma.Models.ImageSize;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentTypeItem;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.ApartmentsViewModel;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.RequiermentsViewModel;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Services.UploadImageService;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.budimir.danilo.gradjevinskafirma.Utils.RealPathUtil;
import com.budimir.danilo.gradjevinskafirma.Utils.RetrofitClient;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.relex.circleindicator.CircleIndicator;

public class AddReqiermentFragment extends Fragment {
    private static final int REQUEST_IMAGE_cAPTURE = 101;
    private static int numSlides = 0;


    private int currentPage;
    ProgressDialog dialg;
    private ArrayList<String> imagesNames = new ArrayList<>();
    private ArrayList<String> imagesUrl;
    IuploadAPI iuploadAPI;
    int mStackLevel = 0;
    private ImageBotomModal modal;
    private int numOfImages;
    private SliderAdapter sliderAdapter;



    public AddReqiermentFragment() {

        this.numOfImages = 0;
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_reqierment, container, false);
        ArrayList<RequiermentTypeItem> requierment_types = new ArrayList();
        requierment_types.add(new RequiermentTypeItem(1, "Kupatilo"));
        requierment_types.add(new RequiermentTypeItem(2, "Ulazna Vrata"));

        ApartmentsViewModel apartments_model = ViewModelProviders.of(getActivity()).get(ApartmentsViewModel.class);
        apartments_model.getChooseApartmentItem().observe(this, new Observer<ArrayList<ChooseApartmentItem>>() {
            @Override
            public void onChanged(@Nullable ArrayList<ChooseApartmentItem> items) {

                ((Spinner) view.findViewById(R.id.requierment_choose_apartment)).setAdapter(new ChooseApartmentSpinnerAdapter(AddReqiermentFragment.this.getContext(), R.layout.choose_apartment_spiner_item, items));

            }
        });

        ((Spinner) view.findViewById(R.id.choose_requierment_type_name)).setAdapter(new ChooseRequiermentTypeAdapter(view.getContext(), R.layout.choose_requierment_type_item, requierment_types));
        final ViewPager slider = (ViewPager) view.findViewById(R.id.slider);

        this.imagesUrl = new ArrayList();
        this.sliderAdapter = new SliderAdapter(view.getContext(), this.imagesUrl);
        slider.setAdapter(this.sliderAdapter);
        slider.setBackgroundResource(R.drawable.ic_add_a_photo_black_24dp);
        ((CircleIndicator) view.findViewById(R.id.indicator)).setViewPager(slider);
        ((Button) view.findViewById(R.id.submit)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Log.d("SUBMITED","Okinuto");

                EditText requestDescription = (EditText) view.findViewById(R.id.requierment_description);
                ChooseApartmentItem selsectedApartment = (ChooseApartmentItem) ((Spinner) view.findViewById(R.id.requierment_choose_apartment)).getSelectedItem();
                try {
                    JSONObject jsonRequierment = prepareJsonToSend(selsectedApartment.getApartmentId(), ((EditText) view.findViewById(R.id.requierment_title)).getText().toString(), requestDescription.getText().toString(), AddReqiermentFragment.this.imagesNames);

                    SendRequiermentRequest.getInstance().sendRequierment(jsonRequierment, AddReqiermentFragment.this.getContext(), new SendRequiermentCallBack() {
                        @Override
                        public void send_requierment_call_back(RequiermentListItem item, boolean succeed) {

                                if (succeed){
                                    ((ProfileActivity) Objects.requireNonNull(getActivity())).setRequiermentFragmentAfterUpload();
                                    imagesNames.clear();
                                    startUploadServices(item);
                                }

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        });
        slider.addOnPageChangeListener(new OnPageChangeListener() {
            public void onPageScrolled(int i, float v, int i1) {
            }
            public void onPageSelected(int i) {
                AddReqiermentFragment.this.currentPage = i;
            }

            public void onPageScrollStateChanged(int i) {

            }
        });
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run() {
                if (AddReqiermentFragment.this.currentPage == AddReqiermentFragment.numSlides) {
                    AddReqiermentFragment.this.currentPage = 0;
                    slider.setCurrentItem(AddReqiermentFragment.this.currentPage, true);
                }else
                {
                    slider.setCurrentItem(AddReqiermentFragment.this.currentPage = AddReqiermentFragment.this.currentPage + 1, true);
                }

            }
        };
        new Timer().schedule(new TimerTask() {
            public void run() {
                handler.post(update);
            }
        }, 1500, 3000);


        ((Button) view.findViewById(R.id.addImage)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AddReqiermentFragment.this.modal = new ImageBotomModal();
                AddReqiermentFragment.this.modal.setTargetFragment(AddReqiermentFragment.this, 555);
                AddReqiermentFragment.this.modal.show(((FragmentActivity) Objects.requireNonNull(AddReqiermentFragment.this.getActivity())).getSupportFragmentManager(), "modal");
            }
        });
        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == -1) {
            Bundle arg = getArguments();
            this.modal.dismiss();
            this.imagesUrl.add(optimizeImage(arg.getString("path")));
            this.sliderAdapter.notifyDataSetChanged();
            numSlides++;
        } else if (requestCode == 102 && resultCode == -1) {
            this.modal.dismiss();
            if (data.getData() != null) {
                Uri uri = data.getData();
                this.imagesUrl.add(optimizeImage(uri));
                this.sliderAdapter.notifyDataSetChanged();
                numSlides++;
                return;
            }
            ClipData mClipData = data.getClipData();
            for (int i = 0; i < mClipData.getItemCount(); i++) {
                this.imagesUrl.add(optimizeImage(mClipData.getItemAt(i).getUri()));
                this.sliderAdapter.notifyDataSetChanged();
                numSlides++;
            }
        } else if (requestCode == 103 && resultCode == -1) {
            Toast.makeText(getContext(), "Images uploaded", Toast.LENGTH_SHORT).show();
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(requestCode);
            stringBuilder.append(" ovo je rqkvest  kod ");
            Log.d("PovratniKod", stringBuilder.toString());
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        imagesNames = new ArrayList<>();
    }

    public void onDetach() {
        super.onDetach();
        imagesNames = null;
    }

    public void onPause() {
        if (HttpSinglton.getInstance(getContext()).getRequestQueue() != null) {
            RequestQueue requestQueue = HttpSinglton.getInstance(getContext()).getRequestQueue();
            getClass();
            requestQueue.cancelAll((Object) "ADD_REQUIERMENT_GET_APARTMENTS");
        }
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("level", this.mStackLevel);
    }

    public String generateRandomString() {
        SecureRandom secureRandom = new SecureRandom();
        String CHARACTERS = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz123456789";
        StringBuilder generatedString = new StringBuilder();
        for (int i = 0; i < 30; i++) {
            generatedString.append(CHARACTERS.charAt(secureRandom.nextInt(CHARACTERS.length())));
        }
        return generatedString.toString();
    }

    public String optimizeImage(String imagePath) {
        File appGallerydir = new File(Environment.getExternalStorageDirectory(), "Apartments");
        if (!appGallerydir.exists()) {
            appGallerydir.mkdir();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss.SSS", Locale.getDefault()).format(new Date());
        StringBuilder imageName = new StringBuilder();
        imageName.append("IMG_");
        imageName.append(timeStamp);
        imageName.append(generateRandomString());
        imageName.append(".jpg");
        File image_to_save = new File(appGallerydir, imageName.toString());
        try {
            Options options = new Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(new File(imagePath)), null, options);
            Bitmap img1 = BitmapFactory.decodeStream(new FileInputStream(new File(imagePath)));
            ImageSize reducedImageSize = reduceSizeOfImage(options.outWidth, options.outHeight);
            Bitmap img = Bitmap.createScaledBitmap(img1, reducedImageSize.getWidth(), reducedImageSize.getHeight(), false);
            try {
                img.compress(CompressFormat.JPEG, 70, new FileOutputStream(image_to_save));
                this.imagesNames.add(image_to_save.getName());
                if (VERSION.SDK_INT >= 19) {
                    Intent scanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                    scanIntent.setData(Uri.fromFile(image_to_save));
                    getActivity().sendBroadcast(scanIntent);
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("file://");
                    stringBuilder.append(Environment.getExternalStorageDirectory());
                    getActivity().sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse(stringBuilder.toString())));
                }
                return image_to_save.getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String optimizeImage(Uri imagePath) {
        File appGallerydir = new File(Environment.getExternalStorageDirectory(), "Apartments");
        if (!appGallerydir.exists()) {
            appGallerydir.mkdir();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        StringBuilder imageName = new StringBuilder();
        imageName.append("IMG_");
        imageName.append(timeStamp);
        imageName.append(generateRandomString());
        imageName.append(".jpg");
        File image_to_save = new File(appGallerydir, imageName.toString());
        try {
            Options options = new Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(new File(RealPathUtil.getRealPathFromURI_API19(getContext(), imagePath))), null, options);
            Bitmap img1 = BitmapFactory.decodeStream(new FileInputStream(new File(RealPathUtil.getRealPathFromURI_API19(getContext(), imagePath))));
            ImageSize reducedImageSize = reduceSizeOfImage(options.outWidth, options.outHeight);
            Bitmap img = Bitmap.createScaledBitmap(img1, reducedImageSize.getWidth(), reducedImageSize.getHeight(), false);
            try {
                img.compress(CompressFormat.JPEG, 70, new FileOutputStream(image_to_save));
                this.imagesNames.add(image_to_save.getName());
                if (VERSION.SDK_INT >= 19) {
                    Intent scanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                    scanIntent.setData(Uri.fromFile(image_to_save));
                    getActivity().sendBroadcast(scanIntent);
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("file://");
                    stringBuilder.append(Environment.getExternalStorageDirectory());
                    getActivity().sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse(stringBuilder.toString())));
                }
                return image_to_save.getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public ImageSize reduceSizeOfImage(int width, int height) {
        if (height >= 3500) {
            return new ImageSize((int) (((double) width) * 0.35d), (int) (((double) height) * 0.35d));
        }
        if (height >= PathInterpolatorCompat.MAX_NUM_POINTS) {
            return new ImageSize((int) (((double) width) * 0.4d), (int) (((double) height) * 0.4d));
        }
        if (height >= DefaultRetryPolicy.DEFAULT_TIMEOUT_MS) {
            return new ImageSize((int) (((double) width) * 0.5d), (int) (((double) height) * 0.5d));
        }
        if (height >= 2000) {
            return new ImageSize((int) (((double) width) * 0.6d), (int) (((double) height) * 0.6d));
        }
        if (height >= 1700) {
            return new ImageSize((int) (((double) width) * 0.8d), (int) (((double) height) * 0.8d));
        }
        if (height >= 1500) {
            return new ImageSize((int) (((double) width) * 0.9d), (int) (((double) height) * 0.9d));
        }
        return new ImageSize(width, height);
    }




    private JSONObject prepareJsonToSend(int apartmentId, String requiermentTitle, String description,ArrayList<String> imagesNames) throws JSONException {
        JSONObject requiermentJson = new JSONObject();
        requiermentJson.put("apartment_id", apartmentId);
        requiermentJson.put("requierment_title", requiermentTitle);
        requiermentJson.put("requierment_description", description);
        requiermentJson.put("images", new JSONArray(imagesNames));

        return requiermentJson;
    }


    public void startUploadServices(RequiermentListItem item){

        Bundle forImagesUpload = new Bundle();
        forImagesUpload.putStringArrayList("imagesForUpload", AddReqiermentFragment.this.imagesUrl);
        forImagesUpload.putSerializable("item",item);
        Intent uploadIntent = new Intent(AddReqiermentFragment.this.getActivity(), UploadImageService.class);
        uploadIntent.putExtras(forImagesUpload);
        ContextCompat.startForegroundService(AddReqiermentFragment.this.getContext(),uploadIntent);


    }

}
