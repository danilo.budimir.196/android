package com.budimir.danilo.gradjevinskafirma.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.budimir.danilo.gradjevinskafirma.Activitys.ProfileActivity;
import com.budimir.danilo.gradjevinskafirma.Adapters.ImagePrviewAdapter;
import com.budimir.danilo.gradjevinskafirma.Models.ImagesForImagesPager;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.SingleRequiermentViewModel;
import com.budimir.danilo.gradjevinskafirma.R;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class ImagePreviewFragment extends Fragment {

    private SingleRequiermentViewModel model;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(getActivity()).get(SingleRequiermentViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_image_preview, container, false);


                model.getImages().observe(ImagePreviewFragment.this, new Observer<ImagesForImagesPager>() {
                    @Override
                    public void onChanged(@Nullable final ImagesForImagesPager imagesForImagesPager) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ViewPager imagePager =view.findViewById(R.id.imagePreviewPager);
                                        ImagePrviewAdapter adapter = new ImagePrviewAdapter(view.getContext(),imagesForImagesPager.getImages());
                                        imagePager.setAdapter(adapter);
                                        imagePager.setCurrentItem(imagesForImagesPager.getItem());

                                    }
                                });

                            }






                    });





        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
