package com.budimir.danilo.gradjevinskafirma.Interfaces;

public interface FcmCallBacks {

    void getFcmIdCallBack(String fcm_id);
}
