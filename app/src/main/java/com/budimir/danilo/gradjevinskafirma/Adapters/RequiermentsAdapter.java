package com.budimir.danilo.gradjevinskafirma.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import java.util.ArrayList;

public class RequiermentsAdapter extends ArrayAdapter<RequiermentListItem> {
    private Context mContext;
    private ArrayList<RequiermentListItem> mObjects;

    public RequiermentsAdapter(@NonNull Context context, @LayoutRes int layoutRes, @NonNull ArrayList<RequiermentListItem> objects) {
        super(context, layoutRes, objects);
        this.mObjects = objects;
        this.mContext = context;
    }

    public int getCount() {
        return this.mObjects.size();
    }

    public RequiermentListItem getItem(int position) {
        return (RequiermentListItem) this.mObjects.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService("layout_inflater");
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.requierments_item, null, true);
        }
        if (getCount() > 0) {
            Log.d("Lista",mObjects.toString());
            ImageView requiermentImage = (ImageView) convertView.findViewById(R.id.requierment_pic);
            TextView requiermentTitle = (TextView) convertView.findViewById(R.id.requiermentTitle);
            TextView aparttmentName = (TextView) convertView.findViewById(R.id.requierment_apartmentName);
            NetworkImageView requierUserImg = (NetworkImageView) convertView.findViewById(R.id.requierment_user_img);
            TextView requierUserInfo = (TextView) convertView.findViewById(R.id.user_name_surname_requierment);
            requierUserImg.setDefaultImageResId(R.drawable.avatar_img);
            requiermentImage.setImageResource(R.drawable.stan_example);
            requiermentTitle.setText(((RequiermentListItem) this.mObjects.get(position)).getRequiermentTitele());
            aparttmentName.setText(((RequiermentListItem) this.mObjects.get(position)).getApartmentname());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(ConectionInfo.getImagePath());
            stringBuilder.append(((RequiermentListItem) this.mObjects.get(position)).getUserImage());
            requierUserImg.setImageUrl(stringBuilder.toString(), HttpSinglton.getInstance(getContext()).getImageLoader());
            stringBuilder = new StringBuilder();
            stringBuilder.append(((RequiermentListItem) this.mObjects.get(position)).getRequierUsername());
            stringBuilder.append(" ");
            stringBuilder.append(((RequiermentListItem) this.mObjects.get(position)).getRequierSurname());
            requierUserInfo.setText(stringBuilder.toString());



        }
        return convertView;
    }
}
