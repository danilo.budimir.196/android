package com.budimir.danilo.gradjevinskafirma.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.budimir.danilo.gradjevinskafirma.R;
import java.io.File;
import java.io.IOException;

@SuppressLint({"ValidFragment"})
public class ImageBotomModal extends BottomSheetDialogFragment {
    private final int IMAGE_CHOOSER_CODE = 102;
    private String imageFilePath;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_modal, container, true);
        if (!(ContextCompat.checkSelfPermission(getContext(), "android.permission.WRITE_EXTERNAL_STORAGE") == 0 || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), "android.permission.WRITE_EXTERNAL_STORAGE"))) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        }
        ((ImageButton) view.findViewById(R.id.startCamera)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ImageBotomModal.this.openCameraIntent();
            }
        });
        ((ImageButton) view.findViewById(R.id.openImageChooser)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ImageBotomModal.this.openImageChooser();
            }
        });
        return view;
    }

    private void openCameraIntent() {
        Intent cammeraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (cammeraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Log.d("create_Image_exeption", e.getMessage());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (photoFile != null) {
                cammeraIntent.putExtra("output", FileProvider.getUriForFile(getContext(), "com.budimir.danilo.gradjevinskafirma.provider", photoFile));
                Fragment fragmet = getTargetFragment();
                if (fragmet != null) {
                    Bundle bund = new Bundle();
                    bund.putString("path", photoFile.getAbsolutePath());
                    fragmet.setArguments(bund);
                    fragmet.startActivityForResult(cammeraIntent, 101);
                }
            }
        }
    }

    private File createImageFile() throws IOException, Exception {
        File imageTosave = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "IMG_slika.jpg");
        if (imageTosave.exists()) {
            imageTosave.delete();
        } else {
            imageTosave.createNewFile();
        }
        Log.d("TMP_IMAGE_PATH", imageTosave.getAbsolutePath());
        return imageTosave;
    }

    private void openImageChooser() {
        Intent image_chooser = new Intent();
        image_chooser.setType("image/*");
        image_chooser.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
        image_chooser.setAction("android.intent.action.GET_CONTENT");
        if (image_chooser.resolveActivity(getActivity().getPackageManager()) != null) {
            Fragment targetFragment = getTargetFragment();
            if (targetFragment != null) {
                targetFragment.startActivityForResult(image_chooser, 102);
            }
        }
    }
}
