package com.budimir.danilo.gradjevinskafirma.Models;

public class ApartmentListItem {
    private String apartment_addres;
    private String apartment_name;
    private int apartment_id;

    public ApartmentListItem(String apartment_name, String apartment_addres) {
        this.apartment_name = apartment_name;
        this.apartment_addres = apartment_addres;
    }


    public ApartmentListItem(String apartment_addres, String apartment_name, int apartment_id) {
        this.apartment_addres = apartment_addres;
        this.apartment_name = apartment_name;
        this.apartment_id = apartment_id;
    }

    public int getApartment_id() {
        return apartment_id;
    }

    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }

    public String getApartment_name() {
        return this.apartment_name;
    }

    public void setApartment_name(String apartment_name) {
        this.apartment_name = apartment_name;
    }

    public String getApartment_addres() {
        return this.apartment_addres;
    }

    public void setApartment_addres(String apartment_addres) {
        this.apartment_addres = apartment_addres;
    }
}
