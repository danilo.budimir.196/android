package com.budimir.danilo.gradjevinskafirma.HttpRequests.AddRequiermentsRequests;

import android.content.Context;
import android.view.textclassifier.TextLinks;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Interfaces.LoadApartmentsListCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.ChooseApartmentItem;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class LoadApartmentsListRequest {

    private final String GET_APARTMENTS_LIST_URL = ConectionInfo.getConnAddres()+"/addRequiermentsGetApartments";
    private final String GET_APARTMENTS_LIST_TAG = "GET_APARTMENTS_LIST";
    private static LoadApartmentsListRequest instance = null;

    private LoadApartmentsListRequest(){

    }

    public static synchronized LoadApartmentsListRequest getInstance(){

        if (instance == null) instance=new LoadApartmentsListRequest();
        return instance;

    }


    public void pullApartmentList(final Context context, final LoadApartmentsListCallBack callBack){

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, this.GET_APARTMENTS_LIST_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("is_suecses")){
                        final List<ChooseApartmentItem> responseList = new ArrayList<>();
                        JSONArray json_response = (JSONArray) ((JSONObject) response.get("apartments")).get("list");
                        for (int i = 0; i<json_response.length();i++)
                        {
                            responseList.add(new ChooseApartmentItem(((JSONObject) json_response.get(i)).getInt("id"),((JSONObject) json_response.get(i)).getString("apartment_name")));
                        }
                        callBack.getApartmentsListCallBack(responseList);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            public Map getHeaders() throws AuthFailureError{
                HashMap headers = new HashMap();
                Token token = new Token();
                token.id = 1;
                token.funcCaller = TokenCrudCaled.getToken;
                Token info = null;

                try {
                    info = (Token) new TokenAsyncTask(context).execute(token).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                headers.put("Content-Type","application/json");
                headers.put("Authorization",info.token_value);
                return headers;

            }

        };

        request.setTag(this.GET_APARTMENTS_LIST_TAG);
        HttpSinglton.getInstance(context).addToRequestQueue(request);

    }

}
