package com.budimir.danilo.gradjevinskafirma.Models;

public class RequiermentTypeItem {
    private String requierment_type;
    private int requierment_type_id;

    public RequiermentTypeItem(int requierment_type_id, String requierment_type) {
        this.requierment_type_id = requierment_type_id;
        this.requierment_type = requierment_type;
    }

    public int getRequierment_type_id() {
        return this.requierment_type_id;
    }

    public void setRequierment_type_id(int requierment_type_id) {
        this.requierment_type_id = requierment_type_id;
    }

    public String getRequierment_type() {
        return this.requierment_type;
    }

    public void setRequierment_type(String requierment_type) {
        this.requierment_type = requierment_type;
    }
}
