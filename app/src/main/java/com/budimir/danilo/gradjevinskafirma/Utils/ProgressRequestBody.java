package com.budimir.danilo.gradjevinskafirma.Utils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.budimir.danilo.gradjevinskafirma.Interfaces.UploadCallBacks;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class ProgressRequestBody extends RequestBody {
    private static final int DEFAULT_BUFFER_SIZE = 4096;
    private File file;
    private UploadCallBacks listener;

    private class ProgressUpdater implements Runnable {
        private long fileLenght;
        private long uploaded;

        public ProgressUpdater(long uploaded, long fileLenght) {
            this.uploaded = uploaded;
            this.fileLenght = fileLenght;
        }

        public void run() {
            ProgressRequestBody.this.listener.onProgresUpdate((int) ((this.uploaded * 100) / this.fileLenght));
        }
    }

    public ProgressRequestBody(File file, UploadCallBacks listener) {
        this.file = file;
        this.listener = listener;
    }

    @Nullable
    public MediaType contentType() {
        return MediaType.parse("image/*");
    }

    public long contentLength() throws IOException {
        return this.file.length();
    }

    public void writeTo(BufferedSink sink) throws IOException {
      long fileLenght = file.length();
      byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
      FileInputStream in = new FileInputStream(file);
      long uploaded = 0;


      try {
          int read;
          Handler handler = new Handler(Looper.getMainLooper());
          while ((read = in.read(buffer)) != -1)
          {
              handler.post(new ProgressUpdater(uploaded,fileLenght));
              sink.write(buffer,0,read);
          }

      }finally {
          in.close();
      }

    }
}
