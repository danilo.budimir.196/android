package com.budimir.danilo.gradjevinskafirma.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.budimir.danilo.gradjevinskafirma.Activitys.ProfileActivity;
import com.budimir.danilo.gradjevinskafirma.Adapters.RequiermentsGridAdapter;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.RequiermentsRequest.LoadSingleRequiermentRequest;
import com.budimir.danilo.gradjevinskafirma.Models.ImagesForImagesPager;
import com.budimir.danilo.gradjevinskafirma.Models.SingleRequiermentItem;
import com.budimir.danilo.gradjevinskafirma.Models.ViewModel.SingleRequiermentViewModel;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;

import org.w3c.dom.Text;

import java.util.HashMap;


public class SingleRequierment extends Fragment {


    private SingleRequiermentViewModel model;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(getActivity()).get(SingleRequiermentViewModel.class);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_single_requierment, container, false);
        final int id = this.getArguments().getInt("id");
        Log.d("ID_U_SR","id ="+id);
        model.getItems(id).observe(this, new Observer<HashMap<Integer, SingleRequiermentItem>>() {
            @Override
            public void onChanged(@Nullable HashMap<Integer, SingleRequiermentItem> requierments) {

                Log.d("requierments",requierments.toString());
                final SingleRequiermentItem requierment = requierments.get(id);
                if (requierment == null) return;
                NetworkImageView user_image = (NetworkImageView) rootView.findViewById(R.id.requierment_user_img);
                user_image.setImageUrl(ConectionInfo.getImagePath()+requierment.getRequier_image(), HttpSinglton.getInstance(rootView.getContext()).getImageLoader());
                String name_surname = requierment.getRequier_name() + " "+requierment.getRequier_surname();
                TextView name_surname_textView = (TextView) rootView.findViewById(R.id.single_name_surname);
                name_surname_textView.setText(name_surname);
                TextView title = rootView.findViewById(R.id.single_requierment_title);
                title.setText(requierment.getRequierment_title());
                TextView description = rootView.findViewById(R.id.single_requierment_description);
                description.setText(requierment.getRequierment_description());
                GridView gridView = rootView.findViewById(R.id.imagesGrid);


                RequiermentsGridAdapter adapter = new RequiermentsGridAdapter(requierment.getThumb_images(),rootView.getContext());
                gridView.setAdapter(adapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        model.setImages(new ImagesForImagesPager(position,requierment.getImages()));
                        ((ProfileActivity) getActivity()).showImagePrview(position,requierment.getImages());
                    }
                });



            }
        });


        model.loadIfdontExist();

        return rootView;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();
        HttpSinglton.getInstance(getContext()).getRequestQueue().cancelAll(LoadSingleRequiermentRequest.GET_REQUIERMENT_TAG);


    }



}
