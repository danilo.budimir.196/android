package com.budimir.danilo.gradjevinskafirma.Utils;

public class ConectionInfo {
    private static final String connAddres = "http://24.135.73.237:9000";
    private static final String imagePath = "http://24.135.73.237:9000/public/img/";
    private static final String retrofitBaseUrl = "http://24.135.73.237:9000/";

    public static String getConnAddres() {
        return connAddres;
    }

    public static String getRetrofitBaseUrl() {
        return retrofitBaseUrl;
    }

    public static String getImagePath() {
        return imagePath;
    }
}
