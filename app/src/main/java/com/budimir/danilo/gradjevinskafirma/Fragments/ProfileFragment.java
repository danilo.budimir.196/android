package com.budimir.danilo.gradjevinskafirma.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.UserDetailsAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.UserDetailsCrudCaller;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.UserDetails;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;

import java.util.concurrent.ExecutionException;

public class ProfileFragment extends Fragment {
    private final String GET_REQUIERMENTS_TAG = "GET_REQUIERMENTS";
    private final String GET_REQUIERMENTS_URL;
    private String img;
    private String name;
    private String surname;

    public ProfileFragment() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(ConectionInfo.getConnAddres());
        stringBuilder.append("/getRequierments");
        this.GET_REQUIERMENTS_URL = stringBuilder.toString();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle getProfileInfo = getActivity().getIntent().getBundleExtra("profile_information");
        if (getProfileInfo !=null)
        {
            this.name = getProfileInfo.getString("name");
            this.surname = getProfileInfo.getString("surname");
            this.img = getProfileInfo.getString("img");
        }else {

            UserDetails details = new UserDetails();
            details.id = 1;
            details.funcCaller = UserDetailsCrudCaller.getUser;
            UserDetails details1;
            try {
                details1 = new UserDetailsAsyncTask(getContext()).execute(details).get();
                if (details1.executeStatus) {
                    this.name = details1.name;
                    this.surname = details1.surname;
                    this.img = details1.img;
                }

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        TextView nameTextView = (TextView) view.findViewById(R.id.profile_name);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.name);
        stringBuilder.append(" ");
        stringBuilder.append(this.surname);
        nameTextView.setText(stringBuilder.toString());
        NetworkImageView profileImg = (NetworkImageView) view.findViewById(R.id.profile_img_user);
        profileImg.setDefaultImageResId(R.drawable.avatar_img);
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(ConectionInfo.getImagePath());
        stringBuilder2.append(this.img);
        profileImg.setImageUrl(stringBuilder2.toString(), HttpSinglton.getInstance(getContext()).getImageLoader());
        return view;
    }

    public void onDetach() {
        super.onDetach();
    }
}
