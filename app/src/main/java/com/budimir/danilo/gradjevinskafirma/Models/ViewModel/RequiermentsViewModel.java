package com.budimir.danilo.gradjevinskafirma.Models.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.budimir.danilo.gradjevinskafirma.Fragments.Requierments;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.RequiermentsRequest.LoadRequiermentsRequest;
import com.budimir.danilo.gradjevinskafirma.Interfaces.LoadRequiermentsCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;

import java.util.ArrayList;
import java.util.List;

public class RequiermentsViewModel extends AndroidViewModel {

    private ArrayList<RequiermentListItem> requiermentListItems = new ArrayList<>();
    private MutableLiveData<ArrayList<RequiermentListItem>> requiermentsListMutable;
    private MutableLiveData<RequiermentListItem> singleRequierment = new MutableLiveData<>();
    private int recuiermentCount=0;
    private MutableLiveData<Integer> recuiermentCountMutable = new MutableLiveData<>();



    public RequiermentsViewModel(@NonNull Application application) {
        super(application);


    }


    public LiveData<ArrayList<RequiermentListItem>> getRequierments() {

        if(requiermentsListMutable == null)
        {

            this.requiermentsListMutable = new MutableLiveData<ArrayList<RequiermentListItem>>();
            loadRequierments(requiermentsListMutable);



        }

        return requiermentsListMutable;
    }


    private void loadRequierments(final MutableLiveData<ArrayList<RequiermentListItem>> mutable) {

        LoadRequiermentsRequest.getInstance().pullRequierments(super.getApplication().getApplicationContext(), new LoadRequiermentsCallBack() {
            @Override
            public void getRequiermentsCallBack(List<RequiermentListItem> requierments) {

                  requiermentListItems.addAll(requierments);
                  mutable.setValue(requiermentListItems);

            }
        });


    }


    public LiveData<RequiermentListItem> getSingleRequierment()
    {
        return singleRequierment;
    }


    public void setRequierment(RequiermentListItem item){
        singleRequierment.setValue(item);
    }


    public void addRequierment(RequiermentListItem item)
    {
        if (requiermentsListMutable == null)return;
        requiermentListItems.add(0,item);
        requiermentsListMutable.setValue(requiermentListItems);
    }

    public void setRecuiermentCount(int number)
    {
        if (number == 0) {
            recuiermentCount = number;
            recuiermentCountMutable.postValue(recuiermentCount);
        }else {

            recuiermentCount++;
            recuiermentCountMutable.postValue(recuiermentCount);
        }
    }

    public LiveData<Integer> getRecuiermentCount()
    {
        return recuiermentCountMutable;
    }





}
