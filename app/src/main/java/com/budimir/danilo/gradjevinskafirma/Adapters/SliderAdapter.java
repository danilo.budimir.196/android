package com.budimir.danilo.gradjevinskafirma.Adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.budimir.danilo.gradjevinskafirma.R;
import java.util.ArrayList;

public class SliderAdapter extends PagerAdapter {
    private int[] defaultImage;
    private ArrayList<String> images;
    private boolean isDefault;
    private Context mContext;

    public SliderAdapter(Context context, ArrayList<String> images) {
        this.mContext = context;
        this.images = images;
    }

    public int getCount() {
        return this.images.size();
    }

    @NonNull
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = ((Activity) this.mContext).getLayoutInflater().inflate(R.layout.slider_item, container, false);
        container.addView(view);
        if (this.images.size() > 0) {
            ((ImageView) view.findViewById(R.id.slider_image)).setImageURI(Uri.parse((String) this.images.get(position)));
        } else {
            ((ImageView) view.findViewById(R.id.slider_image)).setImageResource(R.drawable.ic_add_a_photo_black_24dp);
        }
        return view;
    }

    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return o == view;
    }
}
