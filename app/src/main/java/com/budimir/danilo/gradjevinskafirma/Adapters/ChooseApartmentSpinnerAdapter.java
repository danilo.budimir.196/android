package com.budimir.danilo.gradjevinskafirma.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.budimir.danilo.gradjevinskafirma.Models.ChooseApartmentItem;
import com.budimir.danilo.gradjevinskafirma.R;
import java.util.ArrayList;

public class ChooseApartmentSpinnerAdapter extends ArrayAdapter {
    private Context mContext;
    private ArrayList<ChooseApartmentItem> mObjects;

    public ChooseApartmentSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<ChooseApartmentItem> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mObjects = objects;
    }

    public int getCount() {
        return this.mObjects.size();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public ChooseApartmentItem getItem(int position) {
        return (ChooseApartmentItem) this.mObjects.get(position);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService("layout_inflater");
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.choose_apartment_spiner_item, null, false);
        }
        if (getCount() > 0) {
            ((TextView) convertView.findViewById(R.id.choose_apartment_name)).setText(((ChooseApartmentItem) this.mObjects.get(position)).getApartment_name());
        }
        return convertView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}
