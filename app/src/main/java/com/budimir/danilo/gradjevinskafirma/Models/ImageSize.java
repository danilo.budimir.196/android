package com.budimir.danilo.gradjevinskafirma.Models;

public class ImageSize {
    private int height;
    private int width;

    public ImageSize(int width, int height) {
        this.height = height;
        this.width = width;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
