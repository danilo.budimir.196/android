package com.budimir.danilo.gradjevinskafirma.DatabaseConf;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Dao.TokenDao;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Dao.UserDetailsDao;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.UserDetails;


@Database(entities = {Token.class, UserDetails.class}, exportSchema = false, version = 2 )
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract TokenDao tokenDao();
    public abstract UserDetailsDao userDetailsDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = (AppDatabase) Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "stanovi").fallbackToDestructiveMigration().build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
