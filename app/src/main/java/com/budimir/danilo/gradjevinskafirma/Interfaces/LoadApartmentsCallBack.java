package com.budimir.danilo.gradjevinskafirma.Interfaces;

import com.budimir.danilo.gradjevinskafirma.Models.ApartmentListItem;

import java.util.ArrayList;
import java.util.List;

public interface LoadApartmentsCallBack {

    void getApartmentsCallBack(List<ApartmentListItem> apartments);
}
