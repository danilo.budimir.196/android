package com.budimir.danilo.gradjevinskafirma.Utils;

import android.util.Log;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class UploadImageUtil {
    private String imagesPath;
    private String token;
    private String url;

    public UploadImageUtil(String imagesPath, String url, String token) {
        this.imagesPath = imagesPath;
        this.url = url;
        this.token = token;
    }

    public void uploadImages() throws IOException {
        URL url = new URL(this.url);
        File sourceFile = new File(this.imagesPath);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        String boundary = "*****";
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setUseCaches(false);
        connection.setRequestMethod("POST");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("multipart/form-data;boundary=");
        stringBuilder.append(boundary);
        connection.setRequestProperty("Content-Type", stringBuilder.toString());
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("ENCTYPE", "multipart/form-data");
        connection.setRequestProperty("img", sourceFile.getName());
        FileInputStream fileInputStream = new FileInputStream(sourceFile);
        DataOutputStream request = new DataOutputStream(connection.getOutputStream());
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("--");
        stringBuilder2.append(boundary);
        stringBuilder2.append("--\r\n");
        request.writeBytes(stringBuilder2.toString());
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Content-Disposition: form-data; name=\"img\"; filename=\"");
        stringBuilder2.append(sourceFile.getName());
        stringBuilder2.append("\"\r\n");
        request.writeBytes(stringBuilder2.toString());
        request.writeBytes("\r\n");
        int bufferSize = Math.min(fileInputStream.available(), 1048576);
        byte[] buffer = new byte[bufferSize];
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while (bytesRead > 0) {
            request.write(buffer, 0, bufferSize);
            bufferSize = Math.min(fileInputStream.available(), 1048576);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        request.writeBytes("\r\n");
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append("--");
        stringBuilder3.append(boundary);
        stringBuilder3.append("--\r\n");
        request.writeBytes(stringBuilder3.toString());
        request.flush();
        request.close();
        BufferedReader response = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuffer stringResponse = new StringBuffer();
        while (true) {
            String readLine = response.readLine();
            String inputLine = readLine;
            if (readLine != null) {
                stringResponse.append(inputLine);
            } else {
                response.close();
                Log.d("HTTP_CON", stringResponse.toString());
                return;
            }
        }
    }

    private static byte[] readFileToByteArray(File file) {
        byte[] bArray = new byte[((int) file.length())];
        try {
            FileInputStream fis = new FileInputStream(file);
            fis.read(bArray);
            fis.close();
        } catch (IOException ioExp) {
            ioExp.printStackTrace();
        }
        return bArray;
    }
}
