package com.budimir.danilo.gradjevinskafirma.Notifications;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class UploadNotification extends Application {

    public static final String CHANNEL_ID = "UPLOAD_CHANNEL";

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
    }

    private void createNotificationChannel(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel uploadChannel = new NotificationChannel(CHANNEL_ID, "UPLOAD IMAGES", NotificationManager.IMPORTANCE_DEFAULT);


            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(uploadChannel);
        }

    }


}
