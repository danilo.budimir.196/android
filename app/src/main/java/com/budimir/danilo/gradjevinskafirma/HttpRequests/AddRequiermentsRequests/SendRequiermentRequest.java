package com.budimir.danilo.gradjevinskafirma.HttpRequests.AddRequiermentsRequests;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.Fragments.AddReqiermentFragment;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Interfaces.SendRequiermentCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class SendRequiermentRequest {

    private final String SEND_REQUIERMENT_TAG = "SEND_REQUIERMENT";
    private final String SEND_REQUIERMENT_URL = ConectionInfo.getConnAddres()+"/insertRequierment";
    private static SendRequiermentRequest instance = null;


    private SendRequiermentRequest() {}


    public static SendRequiermentRequest getInstance()
    {
        if (instance == null) instance = new SendRequiermentRequest();

        return instance;
    }


    public void sendRequierment(JSONObject json , final Context context, final SendRequiermentCallBack callBack){

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, SEND_REQUIERMENT_URL, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("is_success"))
                    {
                        JSONObject responseRequierment = (JSONObject) response.get("requierment");
                        RequiermentListItem item = jsonToRequiermentObject(responseRequierment);
                        if (item!=null) {

                            callBack.send_requierment_call_back(item,response.getBoolean("is_success"));
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                Token token = new Token();
                token.id = 1;
                token.funcCaller = TokenCrudCaled.getToken;
                Token info = null;
                try {
                    info = (Token) new TokenAsyncTask(context).execute(new Token[]{token}).get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", info.token_value);
                return headers;
            }
        };

        request.setTag(SEND_REQUIERMENT_TAG);
        HttpSinglton.getInstance(context).addToRequestQueue(request);



    }


    private RequiermentListItem jsonToRequiermentObject(JSONObject json) {


        RequiermentListItem item = null;
        try {
            int requiermentID = json.getInt("requierment_id");
            String apartmentName = json.getString("apartment_name");
            String requiermentTitle = json.getString("title");
            String user_name = json.getString("user_name");
            String user_surname =  json.getString("user_surname");
            JSONObject image =  json.getJSONObject("user_image");
            String strImage = "";

                if (image.getBoolean("Valid")) {
                    strImage = image.getString("String");
                }

            item = new RequiermentListItem(requiermentID, requiermentTitle, apartmentName, "", strImage, user_name, user_surname);
            return item;
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return item;

    }







}
