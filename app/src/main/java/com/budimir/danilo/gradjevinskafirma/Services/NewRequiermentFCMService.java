package com.budimir.danilo.gradjevinskafirma.Services;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.BrodcastReceivers.NewRequiermentBroadcastReceiver;
import com.budimir.danilo.gradjevinskafirma.BrodcastReceivers.UploadReceiver;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.FCMrequest.UpdateFcmRequest;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;
import com.budimir.danilo.gradjevinskafirma.Notifications.NewRecuiermentNotification;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class NewRequiermentFCMService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        if(applicationInForeground())
        {
            RequiermentListItem item = mapToRequiermentListItem(remoteMessage.getData());
            startBrodacast(item);

        }else {
            RequiermentListItem item = mapToRequiermentListItem(remoteMessage.getData());
            NewRecuiermentNotification.createNotification("NEW_RECUIERMENT","New recuierment",getApplicationContext(),item);
        }

    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Token t = new Token();
        t.funcCaller = TokenCrudCaled.getToken;
        t.id=1;
        try {
            Token t2 =new TokenAsyncTask(getApplicationContext()).execute(t).get();
            if (t2==null)return;
            UpdateFcmRequest.getInstance().updateToken(getApplicationContext(),s);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    private void startBrodacast(RequiermentListItem item)
    {
        Intent broadcastIntnen = new Intent(NewRequiermentBroadcastReceiver.BROADCAST_ACTION);

        broadcastIntnen.putExtra("item",item);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntnen);

    }


    private  RequiermentListItem mapToRequiermentListItem(Map<String,String> map){

        RequiermentListItem litItem = new RequiermentListItem(Integer.parseInt(map.get("requierment_id")),map.get("title"),map.get("apartment_name"),"",map.get("user_image"),map.get("user_name"),map.get("user_surname"));

        return litItem;

    }


    private boolean applicationInForeground() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> services = activityManager.getRunningAppProcesses();
        boolean isActivityFound = false;

        if (services.get(0).processName
                .equalsIgnoreCase(getPackageName()) && services.get(0).importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
            isActivityFound = true;
        }

        return isActivityFound;
    }


}
