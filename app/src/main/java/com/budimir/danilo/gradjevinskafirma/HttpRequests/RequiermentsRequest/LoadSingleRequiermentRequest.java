package com.budimir.danilo.gradjevinskafirma.HttpRequests.RequiermentsRequest;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.Fragments.SingleRequierment;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Interfaces.LoadSingleRequiermentCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.SingleRequiermentItem;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class LoadSingleRequiermentRequest {

    public static final String GET_REQUIERMENT_TAG = "GET_REQUIERMENT_SINGLE";
    private final String GET_REQUIERMENT_URL = ConectionInfo.getConnAddres()+"/getRequierment";
    private static LoadSingleRequiermentRequest instance =null;

    private LoadSingleRequiermentRequest(){}

    public static synchronized LoadSingleRequiermentRequest getInstance()
    {
        if (instance ==null) instance = new LoadSingleRequiermentRequest();
        return instance;
    }



    public void pullRequierment(final Context context, int requierment_id, final LoadSingleRequiermentCallBack callBack) throws JSONException {

        String url = GET_REQUIERMENT_URL + "/"+requierment_id;
        JsonObjectRequest requierment_request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getBoolean("is_success"))
                    {

                        SingleRequiermentItem item = jsonToSingleReuiermentItem(response);
                        callBack.getSingleRequiermentCallBack(item);

                    }else {

                        Toast.makeText(context,response.getString("err_message"),Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            public Map getHeaders() throws AuthFailureError {
                HashMap heders = new HashMap();
                Token token = new Token();
                token.id = 1;
                token.funcCaller = TokenCrudCaled.getToken;
                token.token_value = null;
                try {
                    token = (Token) new TokenAsyncTask(context).execute(new Token[]{token}).get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                heders.put("Content-Type", "application/json");
                heders.put("Authorization", token.token_value);
                return heders;
            }



        };


        requierment_request.setTag(GET_REQUIERMENT_TAG);
        HttpSinglton.getInstance(context).addToRequestQueue(requierment_request);



    }

    public SingleRequiermentItem jsonToSingleReuiermentItem(JSONObject response) throws JSONException {

        int requierment_id = response.getInt("requierment_id");
        String resuierment_title = response.getString("title");
        String requierment_description = response.getString("description");
        int apartment_id = response.getInt("apartment_id");
        String apartment_address = response.getString("apartment_address");
        String apartment_name = response.getString("apartment_name");
        int user_id = response.getInt("user_id");
        String user_name = response.getString("user_name");
        String user_surname = response.getString("user_surname");
        JSONArray imagesJson = response.getJSONArray("requierment_images");
        ArrayList<String> images = new ArrayList<>();
        for (int i = 0; i<imagesJson.length();i++)
        {
            images.add(imagesJson.getString(i));
        }

        JSONObject user_image = response.getJSONObject("user_image");
        Log.d("Slika",user_image.toString());
        String str_user_image="12";
        if (user_image.getBoolean("Valid")) {
            str_user_image = user_image.getString("String");
        }
        Log.d("Slika_str",str_user_image);

        ArrayList thumbs_images = new ArrayList<>();

        for (String image:images) {

            thumbs_images.add("thumbnail_"+image);
        }


        return new SingleRequiermentItem(requierment_id,resuierment_title,requierment_description,user_name,user_surname,user_id,apartment_name,apartment_address,apartment_id,thumbs_images,images,str_user_image);
    }

}
