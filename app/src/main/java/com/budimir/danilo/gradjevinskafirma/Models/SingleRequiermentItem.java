package com.budimir.danilo.gradjevinskafirma.Models;

import java.util.ArrayList;

public class SingleRequiermentItem {
    private int requierment_id;
    private String requierment_title;
    private String requierment_description;
    private String requier_name;
    private String requier_surname;
    private int requier_id;
    private String requier_image;
    private String apartment_name;
    private String apartment_street;
    private int apartment_id;
    private ArrayList<String> thumb_images;
    private ArrayList<String> images;


    public SingleRequiermentItem() {
    }

    public String getRequier_image() {
        return requier_image;
    }

    public void setRequier_image(String requier_image) {
        this.requier_image = requier_image;
    }

    public SingleRequiermentItem(int requierment_id, String requierment_title, String requierment_description, String requier_name, String requier_surname, int requier_id, String apartment_name, String apartment_street, int apartment_id, ArrayList<String> thumb_images, ArrayList<String> images, String requier_image) {
        this.requierment_id = requierment_id;
        this.requierment_title = requierment_title;
        this.requierment_description = requierment_description;
        this.requier_name = requier_name;
        this.requier_surname = requier_surname;
        this.requier_id = requier_id;
        this.requier_image = requier_image;
        this.apartment_name = apartment_name;
        this.apartment_street = apartment_street;
        this.apartment_id = apartment_id;
        this.thumb_images = thumb_images;
        this.images = images;
    }

    public String getApartment_name() {
        return apartment_name;
    }

    public void setApartment_name(String apartment_name) {
        this.apartment_name = apartment_name;
    }

    public String getApartment_street() {
        return apartment_street;
    }

    public void setApartment_street(String apartment_street) {
        this.apartment_street = apartment_street;
    }

    public int getApartment_id() {
        return apartment_id;
    }

    public void setApartment_id(int apartment_id) {
        this.apartment_id = apartment_id;
    }

    public int getRequierment_id() {
        return requierment_id;
    }

    public void setRequierment_id(int requierment_id) {
        this.requierment_id = requierment_id;
    }

    public String getRequierment_title() {
        return requierment_title;
    }

    public void setRequierment_title(String requierment_title) {
        this.requierment_title = requierment_title;
    }

    public String getRequierment_description() {
        return requierment_description;
    }

    public void setRequierment_description(String requierment_description) {
        this.requierment_description = requierment_description;
    }

    public String getRequier_name() {
        return requier_name;
    }

    public void setRequier_name(String requier_name) {
        this.requier_name = requier_name;
    }

    public String getRequier_surname() {
        return requier_surname;
    }

    public void setRequier_surname(String requier_surname) {
        this.requier_surname = requier_surname;
    }

    public int getRequier_id() {
        return requier_id;
    }

    public void setRequier_id(int requier_id) {
        this.requier_id = requier_id;
    }

    public ArrayList<String> getThumb_images() {
        return thumb_images;
    }

    public void setThumb_images(ArrayList<String> thumb_images) {
        this.thumb_images = thumb_images;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "SingleRequiermentItem{" +
                "requierment_id=" + requierment_id +
                ", requierment_title='" + requierment_title + '\'' +
                ", requierment_description='" + requierment_description + '\'' +
                ", requier_name='" + requier_name + '\'' +
                ", requier_surname='" + requier_surname + '\'' +
                ", requier_id=" + requier_id +
                ", apartment_name='" + apartment_name + '\'' +
                ", apartment_street='" + apartment_street + '\'' +
                ", apartment_id=" + apartment_id +
                ", thumb_images=" + thumb_images +
                ", images=" + images +
                '}';
    }
}
