package com.budimir.danilo.gradjevinskafirma.HttpRequests.ApartmentsRequests;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.Fragments.ListFragment;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Interfaces.LoadApartmentsCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.ApartmentListItem;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class LoadApartmentsRequests {


    private final String GET_APARTMENTS_URL =  ConectionInfo.getConnAddres() +"/getApartments";
    private final String GET_APARTMENTS_TAG = "GET_STANOVI";
    private static LoadApartmentsRequests instance = null;

    private LoadApartmentsRequests()
    {

    }

    public static synchronized LoadApartmentsRequests getInstance()
    {
        if (instance==null) instance = new LoadApartmentsRequests();
        return instance;

    }


    public void pullApartments(final Context context,final LoadApartmentsCallBack callBack)
    {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, this.GET_APARTMENTS_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getBoolean("is_suecses")){
                        final List<ApartmentListItem> responseList = new ArrayList<>();
                        JSONArray json_response = (JSONArray)((JSONObject) response.get("apartments")).get("ListOfApartments");

                        for(int i = 0;i<json_response.length();i++)
                        {
                            ApartmentListItem listItem = new ApartmentListItem(((JSONObject) json_response.get(i)).getString("address"),(((JSONObject) json_response.get(i)).getString("apartment_name")),((JSONObject)json_response.get(i)).getInt("apartment_id"));
                            responseList.add(listItem);
                        }
                        callBack.getApartmentsCallBack(responseList);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                Token token = new Token();
                token.id = 1;
                token.funcCaller = TokenCrudCaled.getToken;
                Token info = null;
                try {
                    info = (Token) new TokenAsyncTask(context).execute(token).get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", info.token_value);
                return headers;
            }


        };

        request.setTag(GET_APARTMENTS_TAG);
        HttpSinglton.getInstance(context).addToRequestQueue(request);
    }


    public void rewoveFromQue(Context context){

        if(HttpSinglton.getInstance(context).getRequestQueue() != null)
        {
            HttpSinglton.getInstance(context).getRequestQueue().cancelAll(GET_APARTMENTS_TAG);
        }
    }


}
