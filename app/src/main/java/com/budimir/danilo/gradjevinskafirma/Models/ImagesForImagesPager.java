package com.budimir.danilo.gradjevinskafirma.Models;

import java.util.ArrayList;

public class ImagesForImagesPager {
    private int item;
    private ArrayList<String> images;

    public ImagesForImagesPager(int item, ArrayList<String> images) {
        this.item = item;
        this.images = images;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
