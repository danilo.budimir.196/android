package com.budimir.danilo.gradjevinskafirma.Models;

public class ChooseApartmentItem {
    private int apartmentId;
    private String apartment_name;

    public ChooseApartmentItem(int apartmentId, String apartment_name) {
        this.apartmentId = apartmentId;
        this.apartment_name = apartment_name;
    }

    public int getApartmentId() {
        return this.apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    public String getApartment_name() {
        return this.apartment_name;
    }

    public void setApartment_name(String apartment_name) {
        this.apartment_name = apartment_name;
    }
}
