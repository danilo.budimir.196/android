package com.budimir.danilo.gradjevinskafirma.Activitys;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.Interfaces.IuploadAPI;
import com.budimir.danilo.gradjevinskafirma.Interfaces.UploadCallBacks;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.budimir.danilo.gradjevinskafirma.Utils.ProgressRequestBody;
import com.budimir.danilo.gradjevinskafirma.Utils.RetrofitClient;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import okhttp3.MultipartBody.Part;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadAactivity extends AppCompatActivity implements UploadCallBacks {
    private int NumofImages;
    private final String UPLOAD_IMAGE_URL = ConectionInfo.getRetrofitBaseUrl();
    IuploadAPI iuploadAPI;
    ProgressBar progressBar;
    private int uploadedImages = 0;

    private IuploadAPI getIuploadAPI() {
        return (IuploadAPI) RetrofitClient.getClient(this.UPLOAD_IMAGE_URL).create(IuploadAPI.class);
    }

    /* Access modifiers changed, original: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_upload_aactivity);
        this.iuploadAPI = getIuploadAPI();
        this.progressBar = (ProgressBar) findViewById(R.id.progresUpload);
        ArrayList<String> imagePaths = getIntent().getExtras().getStringArrayList("imagesForUpload");
        this.NumofImages = imagePaths.size();
        Iterator it = imagePaths.iterator();
        while (it.hasNext()) {
            String path = (String) it.next();
            this.progressBar.setProgress(0);
            uploadImage(path);
            this.uploadedImages++;
            this.progressBar.setProgress(100);
            setResult(103);
            finish();
        }
    }

    private void uploadImage(String imagePath) {
        File file = new File(imagePath);
        final Part body = Part.createFormData("img", file.getName(), new ProgressRequestBody(file, this));
        Token token = new Token();
        token.id = 1;
        token.funcCaller = TokenCrudCaled.getToken;
        Token info = null;
        try {
            info = (Token) new TokenAsyncTask(this).execute(new Token[]{token}).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        final Token finalInfo = info;
        new Thread(new Runnable() {
            public void run() {
                UploadAactivity.this.iuploadAPI.uploadFile(body, finalInfo.token_value).enqueue(new Callback<String>() {
                    public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                        UploadAactivity uploadAactivity = UploadAactivity.this;
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("Uploaded ");
                        stringBuilder.append(UploadAactivity.this.NumofImages);
                        stringBuilder.append(" /");
                        stringBuilder.append(UploadAactivity.this.uploadedImages);
                        Toast.makeText(uploadAactivity, stringBuilder.toString(), Toast.LENGTH_SHORT).show();
                    }

                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(UploadAactivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    public void onProgresUpdate(int precentage) {
        this.progressBar.setProgress(precentage);
    }


}
