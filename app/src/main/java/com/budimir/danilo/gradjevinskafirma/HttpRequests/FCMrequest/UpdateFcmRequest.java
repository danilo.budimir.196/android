package com.budimir.danilo.gradjevinskafirma.HttpRequests.FCMrequest;

import android.content.Context;
import android.provider.Settings;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class UpdateFcmRequest {

    private static String SEND_NEW_FCM_TOKEN = ConectionInfo.getConnAddres()+"/updatefcmtoken";
    public static String SEND_NEW_FCM_TOKEN_TAG= "UPDATE_FCM_TOKEN";
    private static UpdateFcmRequest instance = null;


    private UpdateFcmRequest(){}


    public synchronized static UpdateFcmRequest getInstance()
    {
        if (instance == null) instance = new UpdateFcmRequest();
        return instance;
    }



    public  void updateToken(final Context context, String token)
    {
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("fcm_id",token);
            jsonObject.put("device_id",android_id);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, SEND_NEW_FCM_TOKEN, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                Token token = new Token();
                token.id = 1;
                token.funcCaller = TokenCrudCaled.getToken;
                Token info = null;
                try {
                    info = (Token) new TokenAsyncTask(context).execute(token).get();
                    if (info == null) return null;
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", info.token_value);
                return headers;
            }


        };


        request.setTag(SEND_NEW_FCM_TOKEN_TAG);
        HttpSinglton.getInstance(context).addToRequestQueue(request);

    }





}
