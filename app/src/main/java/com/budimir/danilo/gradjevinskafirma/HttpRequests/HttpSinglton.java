package com.budimir.danilo.gradjevinskafirma.HttpRequests;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;


public class HttpSinglton {
    private static HttpSinglton instance;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private static Context context;


    private HttpSinglton(Context context){

        this.context = context;
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue, new ImageCache() {

            private final LruCache<String,Bitmap> cache = new LruCache<String, Bitmap>(60);
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }
            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url,bitmap);

            }
        });
    }

    public static synchronized HttpSinglton getInstance(Context context){

        if (instance ==null)
        {
            instance=new HttpSinglton(context);
        }
        return instance;

    }

    public RequestQueue getRequestQueue(){
        if (requestQueue == null)
        {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }
    public <T> void addToRequestQueue(Request<T> request)
    {
        getRequestQueue().add(request);
    }
    public ImageLoader getImageLoader()
    {
        return imageLoader;
    }

}
