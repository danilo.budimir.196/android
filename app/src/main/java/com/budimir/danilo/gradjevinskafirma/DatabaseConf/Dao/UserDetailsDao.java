package com.budimir.danilo.gradjevinskafirma.DatabaseConf.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.UserDetails;

@Dao
public interface UserDetailsDao {
    @Delete
    void delete(UserDetails details);

    @Query("SELECT * FROM UserDetails WHERE id IN (:id)")
    UserDetails getUserDetails(int id);

    @Insert(onConflict =  1)
    long insert(UserDetails details);

    @Update(onConflict = 1)
    int update(UserDetails details);
}
