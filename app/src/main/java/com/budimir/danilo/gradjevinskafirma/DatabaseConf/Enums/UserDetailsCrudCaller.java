package com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums;

public enum  UserDetailsCrudCaller {
    insertUser,
    updateUser,
    getUser,
    deleteUser
}
