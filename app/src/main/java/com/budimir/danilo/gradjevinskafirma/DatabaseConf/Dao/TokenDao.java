package com.budimir.danilo.gradjevinskafirma.DatabaseConf.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;




@Dao
public interface TokenDao {
    @Delete
    void delete(Token token);

    @Query("SELECT * FROM Token WHERE id IN (:id)")
    Token getToken(int id);

    @Insert(onConflict = 1)
    long insert(Token token);

    @Update(onConflict = 1)
    int update(Token token);
}
