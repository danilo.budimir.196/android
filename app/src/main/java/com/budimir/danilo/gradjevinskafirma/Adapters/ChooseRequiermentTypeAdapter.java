package com.budimir.danilo.gradjevinskafirma.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.budimir.danilo.gradjevinskafirma.Models.RequiermentTypeItem;
import com.budimir.danilo.gradjevinskafirma.R;
import java.util.ArrayList;

public class ChooseRequiermentTypeAdapter extends ArrayAdapter {
    private Context mContext;
    private ArrayList<RequiermentTypeItem> mObjects;

    public ChooseRequiermentTypeAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<RequiermentTypeItem> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mObjects = objects;
    }

    public int getCount() {
        return this.mObjects.size();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public RequiermentTypeItem getItem(int position) {
        return (RequiermentTypeItem) this.mObjects.get(position);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService("layout_inflater");
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.choose_requierment_type_item, null, false);
        }
        if (getCount() > 0) {
            ((TextView) convertView.findViewById(R.id.choose_requierment_type)).setText(((RequiermentTypeItem) this.mObjects.get(position)).getRequierment_type());
        }
        return convertView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}
