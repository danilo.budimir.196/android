package com.budimir.danilo.gradjevinskafirma.Models.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.budimir.danilo.gradjevinskafirma.Fragments.SingleRequierment;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.RequiermentsRequest.LoadSingleRequiermentRequest;
import com.budimir.danilo.gradjevinskafirma.Interfaces.LoadSingleRequiermentCallBack;
import com.budimir.danilo.gradjevinskafirma.Models.ImagesForImagesPager;
import com.budimir.danilo.gradjevinskafirma.Models.SingleRequiermentItem;

import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class SingleRequiermentViewModel extends AndroidViewModel {



    public SingleRequiermentViewModel(@NonNull Application application) {
        super(application);
    }

    private  int item_id;
    private MutableLiveData<HashMap<Integer,SingleRequiermentItem>> singleRequiermentListMutable = new MutableLiveData<>();
    private HashMap<Integer,SingleRequiermentItem> singleRequiermentItemHashMap = new HashMap<Integer, SingleRequiermentItem>() ;
    private MutableLiveData<ImagesForImagesPager> images = new MutableLiveData<>();



    private void setSingleRequierment(int id,SingleRequiermentItem item)
    {
        singleRequiermentItemHashMap.put(id,item);
        singleRequiermentListMutable.postValue(singleRequiermentItemHashMap);
    }


    public LiveData<HashMap<Integer,SingleRequiermentItem>> getItems(int id)
    {
       loadSingleRequierment(id);
       return singleRequiermentListMutable;

    }



    private void loadSingleRequierment(final int id)
    {

        try {
            LoadSingleRequiermentRequest.getInstance().pullRequierment(super.getApplication().getApplicationContext(), id, new LoadSingleRequiermentCallBack() {
                @Override
                public void getSingleRequiermentCallBack(SingleRequiermentItem requierment) {

                    setSingleRequierment(requierment.getRequierment_id(),requierment);


                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void setImages(ImagesForImagesPager images)
    {

        this.images.postValue(images);
    }

    public LiveData<ImagesForImagesPager> getImages()
    {
        return this.images;
    }

    public void loadIfdontExist()
    {
        loadSingleRequierment(item_id);
    }

}
