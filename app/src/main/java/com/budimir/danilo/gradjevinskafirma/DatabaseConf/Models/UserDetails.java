package com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.UserDetailsCrudCaller;

import java.io.Serializable;

@Entity
public class UserDetails implements Serializable {

    @Ignore
    public boolean executeStatus;

    @Ignore
    public UserDetailsCrudCaller funcCaller;

    @PrimaryKey
    public int id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "surname")
    public String surname;

    @ColumnInfo(name = "img")
    public String img;


}
