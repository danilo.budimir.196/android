package com.budimir.danilo.gradjevinskafirma.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.toolbox.NetworkImageView;
import com.budimir.danilo.gradjevinskafirma.Activitys.ProfileActivity;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;

import java.util.ArrayList;

public class RequiermentsGridAdapter extends BaseAdapter {


    ArrayList<String> imagesUrl;
    Context mContext;
    public RequiermentsGridAdapter(ArrayList<String> imagesUrl,Context context){
        this.imagesUrl = imagesUrl;
        this.mContext = context;
    }


    @Override
    public int getCount() {
        return imagesUrl.size();
    }

    @Override
    public Object getItem(int position) {
        return imagesUrl.get(position);
    }

    @Override
    public long getItemId(int position) { return 0; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final NetworkImageView image;
        Log.d("DUZINA",this.getCount()+" sdfasdkjasdlksadklassd "+ position);
        if (convertView == null){
            image = new NetworkImageView(mContext);
            image.setLayoutParams(new GridView.LayoutParams(350,350));
            image.setScaleType(NetworkImageView.ScaleType.CENTER_CROP);


        }else {
            image = (NetworkImageView) convertView;

        }
        image.setImageUrl(ConectionInfo.getImagePath()+imagesUrl.get(position), HttpSinglton.getInstance(mContext).getImageLoader());

        return image;

    }
}
