package com.budimir.danilo.gradjevinskafirma.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.AppDatabase;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;

public class TokenAsyncTask extends AsyncTask<Token, Void, Token> {
    private Context currentContex;
    private Token rtnToken = new Token();

    public TokenAsyncTask(Context context) {
        this.currentContex = context;
    }

    /* Access modifiers changed, original: protected|varargs */
    public Token doInBackground(Token... tokens) {
        if (tokens.length > 1) {
            Token token = this.rtnToken;
            token.executeStatus = false;
            return token;
        }
        switch (tokens[0].funcCaller) {
            case insert:
                AppDatabase.getAppDatabase(this.currentContex).tokenDao().insert(tokens[0]);
                AppDatabase.destroyInstance();
                this.rtnToken.executeStatus = true;
                break;
            case delete:
                AppDatabase.getAppDatabase(this.currentContex).tokenDao().delete(tokens[0]);
                AppDatabase.destroyInstance();
                this.rtnToken.executeStatus = true;
                break;
            case update:
                AppDatabase.getAppDatabase(this.currentContex).tokenDao().update(tokens[0]);
                AppDatabase.destroyInstance();
                this.rtnToken.executeStatus = true;
                break;
            case getToken:
                this.rtnToken = AppDatabase.getAppDatabase(this.currentContex).tokenDao().getToken(tokens[0].id);
                this.rtnToken.executeStatus = true;
                AppDatabase.destroyInstance();
                break;
        }
        return this.rtnToken;
    }
}
