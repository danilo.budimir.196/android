package com.budimir.danilo.gradjevinskafirma.Interfaces;

import com.budimir.danilo.gradjevinskafirma.Models.RequiermentListItem;

import java.util.List;

public interface SendRequiermentCallBack {

    void send_requierment_call_back(RequiermentListItem item,boolean succeed);
}
