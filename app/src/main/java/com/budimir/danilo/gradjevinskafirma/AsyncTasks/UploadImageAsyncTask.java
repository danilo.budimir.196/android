package com.budimir.danilo.gradjevinskafirma.AsyncTasks;

import android.os.AsyncTask;
import com.budimir.danilo.gradjevinskafirma.Utils.UploadImageUtil;
import java.io.IOException;

public class UploadImageAsyncTask extends AsyncTask<String, Void, Void> {
    private String token;
    private String url;

    public UploadImageAsyncTask(String url, String token) {
        this.url = url;
        this.token = token;
    }

    /* Access modifiers changed, original: protected|varargs */
    public Void doInBackground(String... strings) {
        for (String value : strings) {
            try {
                new UploadImageUtil(value, this.url, this.token).uploadImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
