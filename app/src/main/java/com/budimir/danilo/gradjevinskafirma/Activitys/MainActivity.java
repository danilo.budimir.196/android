package com.budimir.danilo.gradjevinskafirma.Activitys;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.TokenAsyncTask;
import com.budimir.danilo.gradjevinskafirma.AsyncTasks.UserDetailsAsyncTask;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.UserDetailsCrudCaller;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.Token;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models.UserDetails;
import com.budimir.danilo.gradjevinskafirma.HttpRequests.HttpSinglton;
import com.budimir.danilo.gradjevinskafirma.Interfaces.FcmCallBacks;
import com.budimir.danilo.gradjevinskafirma.R;
import com.budimir.danilo.gradjevinskafirma.Utils.ConectionInfo;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private static final String LoginHttpTag = "Login";
    private byte[] byteToSent;
    private JSONObject jsonToSend = new JSONObject();
    private final String loginUrl;
    private EditText password;
    private Button submit;
    private EditText username;

    public MainActivity() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(ConectionInfo.getConnAddres());
        stringBuilder.append("/login");
        this.loginUrl = stringBuilder.toString();
    }

    /* Access modifiers changed, original: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.username = (EditText) findViewById(R.id.user_name);
        this.password = (EditText) findViewById(R.id.user_password);
        this.submit = (Button) findViewById(R.id.login_submit);
        if (ContextCompat.checkSelfPermission(this, "android.permission.CAMERA") != 0 && !ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.CAMERA")) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.CAMERA"}, 100);
        }
    }

    public void onLoginSubmit(View v) throws JSONException {

        get_fcm_id(new FcmCallBacks() {
            @Override
            public void getFcmIdCallBack(String fcm_id) {
               JSONObject jsonToSend = new JSONObject();
                try {
                    jsonToSend.put("username", username.getText().toString());
                    jsonToSend.put("password", password.getText().toString());
                    jsonToSend.put("fcm_id",fcm_id);
                    jsonToSend.put("device_id",getAndroid_id());
                    sendData(jsonToSend);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });



    }

    public void sendData(JSONObject json) {
        JsonObjectRequest loginRequest = new JsonObjectRequest(1, this.loginUrl, json, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("login")) {
                        Token token = new Token();
                        token.id = 1;
                        token.token_value = response.getString("token");
                        token.funcCaller = TokenCrudCaled.insert;
                        try {
                            if (((Token) new TokenAsyncTask(MainActivity.this).execute(new Token[]{token}).get()).executeStatus) {
                                Bundle forProfileInformation = new Bundle();
                                forProfileInformation.putString("name", response.getString("name"));
                                forProfileInformation.putString("surname", response.getString("surname"));
                                forProfileInformation.putString("img", response.getString("img"));
                                Intent profile = new Intent(MainActivity.this, ProfileActivity.class);
                                profile.putExtra("profile_information", forProfileInformation);
                                UserDetails details = new UserDetails();
                                details.funcCaller= UserDetailsCrudCaller.insertUser;
                                details.id=1;
                                details.name = response.getString("name");
                                details.surname = response.getString("surname");
                                details.img = response.getString("img");

                                new UserDetailsAsyncTask(getApplicationContext()).execute(details);

                                MainActivity.this.startActivity(profile);
                                MainActivity.this.finish();
                            } else {
                                Toast.makeText(MainActivity.this, "nije uredu", Toast.LENGTH_SHORT).show();
                            }
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                        return;
                    }
                    Toast.makeText(MainActivity.this, response.getString("err_message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e3) {
                    e3.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        loginRequest.setTag(LoginHttpTag);
        HttpSinglton.getInstance(this).addToRequestQueue(loginRequest);
    }

    /* Access modifiers changed, original: protected */
    public void onStop() {
        super.onStop();
        if (HttpSinglton.getInstance(this).getRequestQueue() != null) {
            HttpSinglton.getInstance(this).getRequestQueue().cancelAll(LoginHttpTag);
        }
    }


    private void get_fcm_id(final FcmCallBacks callBack)
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(this, new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful())
                {
                    return;
                }

                String fmc_id = task.getResult().getToken();
                callBack.getFcmIdCallBack(fmc_id);
            }
        });
    }

    private String getAndroid_id()
    {
        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        return android_id;
    }

}
