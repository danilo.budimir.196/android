package com.budimir.danilo.gradjevinskafirma.Models;

import java.io.Serializable;

public class RequiermentListItem implements Serializable {
    private String apartmentname;
    private String requierSurname;
    private String requierUsername;
    private int requiermentId;
    private String requiermentImage;
    private String requiermentTitele;
    private String userImage;

    public RequiermentListItem(int requiermentId, String requiermentTitele, String apartmentname, String requiermentImage, String userImage, String requierUsername, String requierSurname) {
        this.requiermentId = requiermentId;
        this.requiermentTitele = requiermentTitele;
        this.apartmentname = apartmentname;
        this.requiermentImage = requiermentImage;
        this.userImage = userImage;
        this.requierUsername = requierUsername;
        this.requierSurname = requierSurname;
    }

    public String getRequiermentTitele() {
        return this.requiermentTitele;
    }

    public void setRequiermentTitele(String requiermentTitele) {
        this.requiermentTitele = requiermentTitele;
    }

    public String getApartmentname() {
        return this.apartmentname;
    }

    public int getRequiermentId() {
        return requiermentId;
    }

    public void setRequiermentId(int requiermentId) {
        this.requiermentId = requiermentId;
    }

    public void setApartmentname(String apartmentname) {
        this.apartmentname = apartmentname;
    }

    public String getRequiermentImage() {
        return this.requiermentImage;
    }

    public void setRequiermentImage(String requiermentImage) {
        this.requiermentImage = requiermentImage;
    }

    public String getUserImage() {
        return this.userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getRequierUsername() {
        return this.requierUsername;
    }

    public void setRequierUsername(String requierUsername) {
        this.requierUsername = requierUsername;
    }

    public String getRequierSurname() {
        return this.requierSurname;
    }

    public void setRequierSurname(String requierSurname) {
        this.requierSurname = requierSurname;
    }

    @Override
    public String toString() {
        return "RequiermentListItem{" +
                "apartmentname='" + apartmentname + '\'' +
                ", requierSurname='" + requierSurname + '\'' +
                ", requierUsername='" + requierUsername + '\'' +
                ", requiermentId=" + requiermentId +
                ", requiermentImage='" + requiermentImage + '\'' +
                ", requiermentTitele='" + requiermentTitele + '\'' +
                ", userImage='" + userImage + '\'' +
                '}';
    }
}
