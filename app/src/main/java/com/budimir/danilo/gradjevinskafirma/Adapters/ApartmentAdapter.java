package com.budimir.danilo.gradjevinskafirma.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.budimir.danilo.gradjevinskafirma.Models.ApartmentListItem;
import com.budimir.danilo.gradjevinskafirma.R;
import io.reactivex.annotations.NonNull;
import java.util.ArrayList;

public class ApartmentAdapter extends ArrayAdapter<ApartmentListItem> {
    private Context mContext;
    private ArrayList<ApartmentListItem> mObjects;

    public ApartmentAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<ApartmentListItem> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mObjects = objects;
    }

    public int getCount() {
        return this.mObjects.size();
    }

    public ApartmentListItem getItem(int position) {
        return (ApartmentListItem) this.mObjects.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService("layout_inflater");
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.apartment_item, null, true);
        }
        if (getCount() > 0) {
            TextView apartment_addres = (TextView) convertView.findViewById(R.id.apartment_addres);
            ImageView apartmen_icon = (ImageView) convertView.findViewById(R.id.apartment_pic);
            ((TextView) convertView.findViewById(R.id.apartment_name)).setText(((ApartmentListItem) this.mObjects.get(position)).getApartment_name());
            apartment_addres.setText(((ApartmentListItem) this.mObjects.get(position)).getApartment_addres());
            apartmen_icon.setImageResource(R.drawable.apartment_icon);
        }
        return convertView;
    }
}
