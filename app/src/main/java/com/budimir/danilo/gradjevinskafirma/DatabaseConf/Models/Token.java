package com.budimir.danilo.gradjevinskafirma.DatabaseConf.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import com.budimir.danilo.gradjevinskafirma.DatabaseConf.Enums.TokenCrudCaled;
import java.io.Serializable;

@Entity
public class Token implements Serializable {
    @Ignore
    public boolean executeStatus;
    @Ignore
    public TokenCrudCaled funcCaller;
    @PrimaryKey
    public int id;
    @ColumnInfo(name = "token_value")
    public String token_value;
}
